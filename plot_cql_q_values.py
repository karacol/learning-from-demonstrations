"""
Script for showing Q-values for trained agents.
Example usage:

python3 plot_cql_q_values.py --task LiftObject --variant vanilla --seed 6002 --demo_idx 0 --device cpu \
    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
    --results_path /home/karacol/Schreibtisch/thesis_results/can_touch/thesis_variants_study_trained
"""


import isaacgym

import argparse
import json
import h5py
import numpy as np
from copy import deepcopy

import torch
import h5py

import robomimic
import robomimic.utils.file_utils as FileUtils
import robomimic.utils.torch_utils as TorchUtils
import robomimic.utils.tensor_utils as TensorUtils

from matplotlib import pyplot as plt
from matplotlib import rc
import matplotlib as mpl
import pyplot_themes as themes

import os
import glob
from tqdm import tqdm
import json

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import pandas as pd

fixed_epochs = ['50', '500', '1000', '2000']

def main(args):
    if args.agent is not None:
        # relative path to agent
        ckpt_paths = [args.agent]
    else:
        ckpt_paths = glob.glob(os.path.join(args.results_path, args.task, args.variant, f"*_seed_{args.seed}_*", "20*", "models", "*"))

    plot_dir = os.path.join(os.path.dirname(os.path.dirname(ckpt_paths[0])), "value_plots")
    try:
        os.mkdir(plot_dir)
    except FileExistsError:
        pass

    #rc('text.latex', preamble=r'\usepackage{cmbright}')
    plt.rcParams.update({
        "text.usetex": True,
        "mathtext.default": "regular",
        "font.family": "serif",
        #"font.sans-serif": ["Computer Modern Serif"],
        "font.size": 14,
        "legend.fontsize": 20,
        "axes.labelpad": 8
    })

    for ckpt_path in ckpt_paths:
        chkpt_dict = FileUtils.load_dict_from_checkpoint(ckpt_path=ckpt_path)
        json_path = os.path.join(plot_dir, f"Q_{chkpt_dict['env_metadata']['env_name']}_{args.variant}")
        if args.all:
            if "model_epoch_" in ckpt_path:
                path_end = ckpt_path.split("/")[-1].split("_")
                epoch_slice = path_end[path_end.index("epoch")+1]
                json_path += f"_epoch_{epoch_slice.split('.')[0]}"    

        if args.demo_idx is not None:
            json_path += f"_demo_{args.demo_idx}"
    data_f = open(json_path+".json", "r")
    data = json.load(data_f)

    df = pca(data["data"])

    ptol_vibrant = ["#0077bb", "#33bbee", "#009988", "#ee7733", "#cc3311", "#ee3377", "#bbbbbb"] #7
    ptol = [ptol_vibrant[i] for i in [1, 3, 0]]

    if not args.all:
        print("Plotting...")
        plot_subplots(df, data["path"], ptol, data["titles"])
        print("Done.")

def pca(data):
    data_for_pd = {"q": [], "a":[], "source": [], "epoch": []}
    for epoch in data.keys():
        for source in data[epoch]["q"].keys():
            n = len(data[epoch]["q"][source])
            data_for_pd["q"] += data[epoch]["q"][source]
            data_for_pd["a"] += data[epoch]["a"][source]
            data_for_pd["source"] += [source]*n
            data_for_pd["epoch"] += [epoch]*n
    df = pd.DataFrame.from_dict(data_for_pd)
    x = np.array(df['a'].to_list())
    x = StandardScaler().fit_transform(x)
    pca = PCA(n_components=2)
    principal_components=pca.fit_transform(x)
    principal_df = pd.DataFrame(data=principal_components, columns=['pc_1', 'pc_2'])
    final_df = pd.concat([principal_df, df[['q', 'source', 'epoch']]], axis=1)
    final_df.info()
    return final_df

def plot_subplots(df, path, colors, titles, q_min=-1000, q_max=0, a_min=-10, a_max=10):
    for azim in [330]:#[30, 60, 90, 120, 150, 210, 240, 270, 300, 330]:
        x_lim = (df['pc_1'].min(), df['pc_1'].max())
        y_lim = (df['pc_2'].min(), df['pc_2'].max())
        z_lim = (df['q'].min(), df['q'].max())
        fig, axes = plt.subplots(1, len(titles), figsize=(4*len(titles), 4), subplot_kw={'projection': '3d'})
        plt.subplots_adjust(wspace=0.4, hspace=0.4)
        N = len(df.loc[(df['epoch'] == '50') & (df['source'] == 'demo')])
        area = (5 * np.ones(N))**2
        ls = []
        for e, epoch in enumerate(fixed_epochs):
            df_epoch = df.loc[df['epoch'] == epoch]
            for i, k in enumerate(["rand", "demo", "policy"]):
                df_curr = df_epoch.loc[df['source'] == k]
                axes[e].view_init(elev=10., azim=azim)
                l = axes[e].scatter(df_curr['pc_1'], df_curr['pc_2'], df_curr['q'], s=area, alpha=0.03, color=colors[i], zorder=i)
                axes[e].grid(True)
                axes[e].set_title(titles[e], fontsize=20)
                if e == 0:
                    ls.append(l)
                axes[e].set_xlabel("PC1")
                axes[e].set_ylabel("PC2")
                axes[e].set_zlabel("Q-value")
                axes[e].set_xlim(x_lim)
                axes[e].set_ylim(y_lim)
                axes[e].set_zlim(z_lim)
                axes[e].tick_params(pad=2)
        leg = plt.legend(ls, ["random action", "demo action", "policy action"], bbox_to_anchor=(0, 1.35, -2.5, 0), loc="center", ncol=3, markerscale=2)
        for lh in leg.legendHandles:
            lh.set_alpha(0.6)
        plt.savefig(path+f".pdf", format='pdf', bbox_inches='tight', transparent=True)
        plt.savefig(os.path.basename(path)+f"_{azim}.pdf", format='pdf', bbox_inches='tight', transparent=True)
        plt.savefig(os.path.basename(path)+f"_{azim}.png", format='png', bbox_inches='tight', transparent=True)
        plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--results_path",
        type=str,
        required=True,
        help="path to results of training (output_dir)",
    )

    parser.add_argument(
        "--task",
        type=str,
        required=True,
        help="task to investigate",
    )

    parser.add_argument(
        "--variant",
        type=str,
        required=True,
        help="variant to investigate",
    )

    parser.add_argument(
        "--seed",
        type=int,
        required=True,
        help="seed to investigate",
    )

    parser.add_argument(
        "--demo",
        type=str,
        required=True,
        help="path to demo file",
    )

    # Path to trained model
    parser.add_argument(
        "--agent",
        type=str,
        help="(optional) path to specific saved checkpoint pth file",
    )

    parser.add_argument(
        "--demo_idx",
        type=int,
        help="(optional) index of demo to play through",
    )    

    parser.add_argument(
        "--device",
        type=str,
        default="cuda:0",
        help="(optional) torch device, default cuda:0",
    )

    parser.add_argument(
        "--all",
        action="store_true",
        help="(optional) run for all epochs instead of only 50, 500, 100, 2000",
    )

    args = parser.parse_args()
    main(args)
