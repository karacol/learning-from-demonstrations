import h5py
import argparse
import numpy as np
from robomimic.utils.file_utils import create_hdf5_filter_key

def select_rewards(hdf5_path, sparse):
    """Adds or adjusts key rewards to be equal to either sparse_rewards or dense_rewards.

    Args:
        hdf5_path (str): path to the hdf5 file
        sparse (bool): if True, use sparse rewards
    """
    f = h5py.File(hdf5_path, "a")
    demos = sorted(list(f["data"].keys()))
    for demo_k in demos:
        rew = f["data"][demo_k]["sparse_rewards"][:] if sparse else f["data"][demo_k]["dense_rewards"][:]
        try:
            f["data"][demo_k].create_dataset('rewards', data=rew)
        except ValueError:  # rewards already exists
            f["data"][demo_k]["rewards"][:] = rew
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset",
        type=str,
        help="path to hdf5 dataset",
    )

    parser.add_argument(
        "--sparse",
        type=bool,
        help="set True if wanting to use sparse rewards, False otherwise",
    )

    args = parser.parse_args()

    select_rewards(args.dataset, args.sparse)