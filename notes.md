# Notes

## sweeps

BC-EXT is not as good as BC-RNN, but BC-RNN can be better than BC (if tuned well).

### BC-EXT sweep

- `gmm.enabled=true` is a bad idea (is not implemented, defaults to BC-RNN-GMM and fails)
- for `mlp=256`
  - values for `exth` sorted by success rate (best to worst): 2, 4, 3, 5, 10
  - values for `exth` sorted by validation loss (lowest to highest): 2, 3, 4, 5, 10 (big difference between first two and rest)
  - success rates more stable than for `mlp=1024`
- for `mlp=1024`
  - values for `exth` sorted by success rate (best to worst): 2, 5, 3, 10, 4 (last three pretty similar)
  - values for `exth` sorted by validation loss (lowest to highest): 2, 3, 4, 5, 10 (not that big a difference)
  - for `exth=2`, significant drop in success rate after 500 episodes whereas `mlp=256` stays stable
    - also visibly higher validation loss


### BC-RNN sweep

- `gmm.enabled=true` does not work somehow (actions output by model are not in [-1, 1])
- 1 vs. 2 RNN layers does not make much difference for small/no MLP
  - bigger difference for 1024 MLP, there 1 layer performs better at the beginning
- the bigger the MLP, the worse the performance over time (overfits)
  - no MLP has low validation loss but gets to high success rates
  - bigger MLPs achieve high success rate earlier those then drop (overfit)
- lower history horizon leads to less validation loss increase
- 4 seems to be a pretty good horizon (quickly achieves high success rate)

### BC sweep

- `gmm.enabled=true` does not work somehow (actions output by model are not in [-1, 1])
- smaller MLPs learn a bit slower but remain more stable over more epochs

## Robomimic configs (paper)

### low-dim

- batch size 100
- num_epochs 2000

### dataset-specific

- machine-generated: validate False

### BC

- learning rate 1e-4
- MLP size (1024, 1024)
- GMM enabled unless machine-generated
- low-dim machine-generated: learning rate 1e-3

### BC-RNN

- horizon 10
- learning rate 1e-4
- actor_layer_dims ()
- rnn hidden dim 400
- GMM enabled unless machine-generated 

### CQL

- batch size 1024 for low_dim
- critic initial learning rate: 1e-3
- actor initial learning rate: 3e-4
- actor target_entropy "default"
- critic deterministic backup True
- critic target q gap 5
- critic min q weight 1
- target tau 5e-3
- discount 0.99
- critic layer dims (300, 400)
- actor layer dims (300, 400)

### hyperparameter ablation study

Was carried out on BC-RNN:
- learning rate to 1e-3
- GMM to False
- MLP to (1024, 1024)
- RNN hidden dim 100

### D4RL

#### CQL

- critic learning rate 3e-4
- actor learning rate 3e-5
- actor bc start steps 40000!!!!!!!!
- critic target q gap None
- critic cql weight 10
- critic min q weight 1
- critic deterministic backup True
- actor layer dims (256, 256, 256)
- critic layer dims (256, 256, 256)