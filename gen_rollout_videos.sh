#!/bin/bash

############################################################################
# camera configs in cfg/task/<task>.yaml:
#
#cameras:
#  save_recordings: True
#  convert_to_pointcloud: False
#  convert_to_voxelgrid: False
#  camera0:
#    type: rgb
#    pos: [ 1, 1, 1.3 ]
#    lookat: [ 0,  0, 0.8 ]
#    horizontal_fov: 90
#    width: 1600
#    height: 900
#
############################################################################

dir=$1/*
pwd=$PWD
model_ct=0

for f in $dir; do
    model_ct=$((model_ct+1))
    model=$f/*/models/model_epoch_2000.pth
    model=$(echo $model)
    video_dir=$f/*/videos
    echo
    echo "*****************************************************************"
    echo $model
    echo "*****************************************************************"
    echo
    for i in {0..4}; do
	echo
	echo "*********************"
	echo $model_ct
	echo $i
	echo "*********************"
	echo
        python3 play_trained.py task=LiftObject headless=True num_envs=1 sim_device=cpu rl_device=cpu demo.play.model_path=$model
        cd $video_dir
	mkdir rollout_$i
        mv $pwd/*color.mp4 rollout_$i
        cd $pwd
    done
done
