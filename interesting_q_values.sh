#!/bin/bash

# Data

# Human
#python3 investigate_cql_q_values.py --task LiftObject --variant vanilla --seed 6002 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 investigate_cql_q_values.py --task LiftObject --variant ext --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 investigate_cql_q_values.py --task LiftObject --variant dv --seed 6130 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 investigate_cql_q_values.py --task LiftObject --variant df --seed 8055 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 investigate_cql_q_values.py --task OpenDoor --variant vanilla --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 investigate_cql_q_values.py --task OpenDoor --variant ext --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 investigate_cql_q_values.py --task OpenDoor --variant dv --seed 6130 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 investigate_cql_q_values.py --task OpenDoor --variant df --seed 8055 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
## Robomimic
#
#
#python3 investigate_cql_q_values.py --task square --variant ext --seed 5596 --device cpu \
#    --demo /home/karacol/git/robomimic/datasets/square/ph/low_dim.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_robomimic_study_trained

# Plots

#python3 plot_cql_q_values.py --task LiftObject --variant vanilla --seed 6002 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 plot_cql_q_values.py --task LiftObject --variant ext --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 plot_cql_q_values.py --task LiftObject --variant dv --seed 6130 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 plot_cql_q_values.py --task LiftObject --variant df --seed 8055 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 plot_cql_q_values.py --task OpenDoor --variant vanilla --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 plot_cql_q_values.py --task OpenDoor --variant ext --seed 5228 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_variants_study_trained
#
#python3 plot_cql_q_values.py --task OpenDoor --variant dv --seed 6130 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained
#
#python3 plot_cql_q_values.py --task OpenDoor --variant df --seed 8055 --device cpu \
#    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/OpenDoor.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_obs_study_trained

# Robomimic

#python3 plot_cql_q_values.py --task square --variant ext --seed 5596 --device cpu \
#    --demo /home/karacol/git/robomimic/datasets/square/ph/low_dim.hdf5 \
#    --results_path /home/karacol/Schreibtisch/thesis_results/thesis_robomimic_study_trained