import h5py
import argparse
import numpy as np
from robomimic.utils.file_utils import create_hdf5_filter_key

def split_sizes_from_hdf5(hdf5_path, p):
    """
    Creates filter key for p% trajectories, named
    "{}_percent".format(p).

    Args:
        hdf5_path (str): path to the hdf5 file

        p (int): percentage of demos to create filter key for
    """

    f = h5py.File(hdf5_path, "r")
    demos = sorted(list(f["data"].keys()))
    f.close()
    
    # get random split
    num_demos = len(demos)
    num_val = int(p/100 * num_demos)
    mask = np.zeros(num_demos)
    mask[:num_val] = 1.
    np.random.shuffle(mask)
    mask = mask.astype(int)
    percent_inds = mask.nonzero()[0]
    percent_keys = [demos[i] for i in percent_inds]

    name = f"{p}_percent"

    percent_lengths = create_hdf5_filter_key(hdf5_path=hdf5_path, demo_keys=percent_keys, key_name=name)

    print("Total number of {}_percent samples: {}".format(p, np.sum(percent_lengths)))
    print("Average number of {}_percent samples {}".format(p, np.mean(percent_lengths)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset",
        type=str,
        help="path to hdf5 dataset",
    )

    args = parser.parse_args()

    # seed to make sure results are consistent
    np.random.seed(0)

    for p in [20, 50]:
        split_sizes_from_hdf5(args.dataset, p)