# Evaluation notes

Might want to use Weights & Biases, can even sync tensorboard stuff.
How to sweep? Grid search? Base parameters and only change one? Random as suggested by wandb?

## datasets

Record 200 trajectories for each:
- human data for all tasks
  - [x] OpenDrawer
  - [x] OpenDoor
  - [x] PourCup
  - [x] LiftObject
- machine-generated data for all tasks
  - [x] OpenDrawer
  - [x] OpenDoor
  - [x] PourCup
  - [x] LiftObject

### comparison of datasets

#### number of demonstrations

##### robomimic

| dataset type | lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- | --- |
| MG | 1 500 | 3 900 | - | - | - |
| PH | 200 | 200 | 200 | 200 | 200 |


##### gym-grasp

| dataset type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| human | 200 | 200 | 200 | 200 |
| human-new | 200 | 200 | 200 | 200 |
| machine-generated | 200 | 200 | 200 | 200 |

#### trajectory length

##### robomimic

| dataset type | lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- | --- |
| MG | $150 \pm 0$ | $150 \pm 0$| - | - | - |
| PH | $46 \pm 6$ | $116 \pm 14$ | $151 \pm 20$ | $469 \pm 54$ | $480 \pm 88$ |
| MH | $104 \pm 44$ | $209 \pm 114$ | $269 \pm 123$ | $653 \pm 201$ | - |

##### gym-grasp

| dataset type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| human | $89 \pm 33$ | $109 \pm 14$ | $151 \pm 27$ | $119 \pm 34$ |
| human-new | $47 \pm 17$ | $73 \pm 15$ | $102 \pm 22$ | $91 \pm 32$ |
| machine-generated | $11 \pm 1$ | $73 \pm 62$ | $35 \pm 50$ | $19 \pm 0$ |

#### number of transitions

##### robomimic

| dataset type | lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- | --- |
| MG | 225 000 | 585 000 | - | - | - |
| PH | 9 666 | 23 207 | 30 154 | 93 752 | 95 962 |

##### gym-grasp

| dataset type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| human | 17 789 | 21 799 | 30 189 | 23 849 |
| human-new | 9 317 | 14 773 | 20 404 | 18 265 |
| machine-generated | 2 257 | 14 647 | 7 006 | 3 800 |

#### observation space

##### robomimic

| observation type | lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- | --- |
| **object**              | 10 | 14 | 14 | 41 | 44 |
| **robot0_eef_pos**      | 3 | 3 | 3 | 3 | 3 |
| **robot0_eef_quat**     | 4 | 4 | 4 | 4 | 4 |
| robot0_eef_vel_ang      | 3 | 3 | 3 | 3 | 3 |
| robot0_eef_vel_lin      | 3 | 3 | 3 | 3 | 3 |
| **robot0_gripper_qpos** | 2 | 2 | 2 | 2 | 2 |
| robot0_gripper_qvel     | 2 | 2 | 2 | 2 | 2 |
| robot0_joint_pos        | 7 | 7 | 7 | 7 | 7 |
| robot0_joint_pos_cos    | 7 | 7 | 7 | 7 | 7 |
| robot0_joint_pos_sin    | 7 | 7 | 7 | 7 | 7 |
| robot0_joint_vel        | 7 | 7 | 7 | 7 | 7 |
| **robot1_eef_pos**      | 0 | 0 | 0 | 3 | 0 |
| **robot1_eef_quat**     | 0 | 0 | 0 | 4 | 0 |
| robot1_eef_vel_ang      | 0 | 0 | 0 | 3 | 0 |
| robot1_eef_vel_lin      | 0 | 0 | 0 | 3 | 0 |
| **robot1_gripper_qpos** | 0 | 0 | 0 | 2 | 0 |
| robot1_gripper_qvel     | 0 | 0 | 0 | 2 | 0 |
| robot1_joint_pos        | 0 | 0 | 0 | 7 | 0 |
| robot1_joint_pos_cos    | 0 | 0 | 0 | 7 | 0 |
| robot1_joint_pos_sin    | 0 | 0 | 0 | 7 | 0 |
| robot1_joint_vel        | 0 | 0 | 0 | 7 | 0 |

| observation type | lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- | --- |
| sum of used | 19 | 23 | 23 | 50 | 53 |

##### gym-grasp

| observation type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| fingertipContactForces  | 15 | 15 | 15 | 15 |
| fingertipPos            | 15 | 15 | 15 | 15 |
| **handDofPos**          | 5  | 5  | 5  | 5  |
| **handPose**            | 7  | 7  | 7  | 7  |
| jointPos                | 17 | 17 | 17 | 17 |
| jointVel                | 17 | 17 | 17 | 17 |
| previousAction          | 11 | 11 | 11 | 11 |
| **drawerOpening**       | 1  | 0  | 0  | 0  |
| handlePos               | 3  | 0  | 0  | 0  |
| **handlePose**          | 0  | 7  | 0  | 0  |
| **fullCupPose**         | 0  | 0  | 7  | 0  |
| emptyCupPose            | 0  | 0  | 7  | 0  |
| **objectPos**           | 0  | 0  | 0  | 3  |
| **objectRot**           | 0  | 0  | 0  | 4  |
| objectLinVel            | 0  | 0  | 0  | 3  |
| objectAngVel            | 0  | 0  | 0  | 3  |

| observation type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| sum of used | 13-16 | 19 | 19 | 19 |

#### action space

##### robomimic

| lift | can | square | transport | tool hang |
| --- | --- | --- | --- | --- |
| 7 | 7| 7 | 14 | 7 |

##### gym-grasp

11

### Batch size restrictions

90-10 train-valid split leads to the following available validation samples, determining the maximum batch size:

| dataset type | OpenDrawer | OpenDoor | PourCup | LiftObject |
| --- | --- | --- | --- | --- |
| human-new | 998 | 1467 | 2228 | 2144 |
| machine-generated | 225 | 1710 | 497 | 380 |

## hyperparamater study

AIRL study suggests that human data may be easier to overfit and report that it benefits from smaller nets, lower LR, regularisation.
How many epochs? 1000?
Use "What matters" configuration as baseline and evaluate the following on our own datasets OpenDoor (H + MG), PourCup (H+MG)(not LiftObject as hardest task and then can check how hyperparameter tuning translates as in "What matters"):
- [x] learning rate
- [x] decaying learning rate
- [x] observation normalisation
- [x] weights/regularisation (L2 regularisation for weight decay, though that can be suboptimal with Adam, see FIXING WEIGHT DECAY REGULARIZATION IN ADAM paper)
- [x] net sizes
- [x] batch sizes (might make sense to keep equal though)

### algorithm-specific

#### RNN

- [x] shared critic RNN vs. non-shared
- [x] recurrent actor
- [x] sequence length (2, 4, 10, 30)
- [x] net sizes
- [x] MLP vs. no MLP

#### EXT

- [x] sequence length
- [x] net sizes (might want smaller nets when sequences given)

### observation ablation study

Also on our data, OpenDoor (H + MG), PourCup (H+MG):
- [ ] actions
- [ ] velocities
- [ ] contact forces

### steps for hyperparameter scan

Prepare the datasets for training, see [README.md](../../README.md#training).

Generate the base configuration files:

```bash
cd cfg/cql_eval
python hyperparam_cfg.py --config cql_base.json --script out.sh --dataset <dataset>
# to use fixed hyperparameters (see script), use:
# python hyperparam_cfg.py --config cql_base.json --script out.sh --dataset <dataset> --fixed
```

Register a new project with a sweep:

```bash
cd <path_to_robomimic>/robomimic/scripts
python prep_hyperparam_scans.py --suite <suite> --variant <variant> --task <task>  # prints Sweep ID and URL
```

In `<path_to_robomimic>/robomimic/scripts/scan_hyperparam.py`, set `base_config_path`, `suite` and `variant`.

Launch the sweeps:

```
cd <path_to_robomimic>/robomimic/scripts
wandb agent --count 50 <entity>/<project>/<sweep_id>
```

### lessons: vanilla variant

- batch dimension of 1024 is too large for OpenDrawer: we don't have enough samples for validation
- max gradient norm None performed better
- actor: smaller networks seemed to work better, but this needs more checking!
- critic: larger networks seemed to work better!
- critic target q gap: 5, 10 perform on par, 1 is bad
- critic use huber has little influence
- actor: decaying learning rate seems to help in the long run!
- critic: non-decaying learning rate looks better

