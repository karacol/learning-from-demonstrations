import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import os
import random

def add_generator_basis(generator, param_dict):
    for k, v in param_dict.items():
        generator.add_param(
            key=k,
            name="",
            group=0,
            values=[v],
        )
    return generator

def history_aware_study(config_file, script_file, data, seeds):
    """Generate config files for CQL history-aware variants study. 2 experiments per task and seed, i.e. 30 in total."""
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # parameters
    basis = {
        "train.output_dir": "../thesis_robomimic_study_trained",
        "experiment.render": False,
        "experiment.render_video": False,
        "experiment.gymgrasp_recording": False,
        "experiment.rollout.n": 20,  # reduced from paper, but it just takes too long o/w
        "algo.ext.history_length": 4,
        "algo.actor.net.rnn.enabled": False,
        "algo.actor.net.rnn.hidden_dim": 256,
        "algo.actor.net.rnn.num_layers": 2,
        "algo.actor.net.rnn.horizon": 4,
        "algo.critic.rnn.hidden_dim": 256,
        "algo.critic.rnn.num_layers": 2,
        "algo.critic.rnn.horizon": 4,
        "algo.critic.rnn.shared": True,
    }
    generator = add_generator_basis(generator, basis)
    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )
    generator.add_param(
        key="train.data",
        name="task", 
        group=2,
        values=list(data.values()),
        value_names=list(data.keys()),
    )

    generator.add_param(
        key="train.seq_length",
        name="variant",
        group=3,
        values=[4, 4],
        value_names=["ext", "rnn"],
    )
    # settings relevant for EXT variant
    generator.add_param(
        key="algo.ext.enabled",
        name="",
        group=3,
        values=[True, False],
    )
    # settings relevant for RNN variant
    generator.add_param(
        key="algo.critic.rnn.enabled",
        name="",
        group=3,
        values=[False, True],
    )

    generator.generate()

def get_data(root):
    datasets = {
        "lift": os.path.join(root, "lift/ph/low_dim.hdf5"),
        "can": os.path.join(root, "can/ph/low_dim.hdf5"),
        "square": os.path.join(root, "square/ph/low_dim.hdf5"),
        "transport": os.path.join(root, "transport/ph/low_dim.hdf5"),
        "tool_hang": os.path.join(root, "tool_hang/ph/low_dim.hdf5")
    }
    return datasets

def main(args):
    data = get_data(args.data_root)
    # choose seed
    seeds = [random.randint(0, 10000) for _ in range(3)]
    history_aware_study(args.config, "robomimic_variants_study.sh", data, seeds)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # path to datasets
    parser.add_argument(
        "--data_root",
        type=str,
        help="root directory containing dataset directories",
    )

    args = parser.parse_args()
    main(args)
