import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import os
import random

def make_generator(config_file, script_file, dataset, seed, task, fixed):
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    #output
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["hyperparam_sweep"],
    )
    # render
    generator.add_param(
        key="experiment.render",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.render_video",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    # rollout
    generator.add_param(
        key="experiment.rollout.enabled",
        name="", 
        group=0, 
        values=[True],
    )
    generator.add_param(
        key="experiment.rollout.n",
        name="", 
        group=0, 
        values=[20],
    )
    generator.add_param(
        key="experiment.rollout.horizon",
        name="", 
        group=0, 
        values=[200],
    )
    # keys
    generator.add_param(
        key="train.dataset_keys",
        name="", 
        group=0, 
        values=[[
            "actions",
            "rewards",
            "dones"
        ]],
    )
    if task == "OpenDrawer":
        generator.add_param(
            key="observation.modalities.obs.low_dim",
            name="task", 
            group=0, 
            values=[[
                "handPose",
                "handDofPos",
                "drawerOpening",
                #"handlePos"
            ]],
            value_names=["OpenDrawer"],
        )
    elif task == "OpenDoor":
        generator.add_param(
            key="observation.modalities.obs.low_dim",
            name="task", 
            group=0, 
            values=[[
                "handPose",
                "handDofPos",
                "handlePose"
            ]],
            value_names=["OpenDoor"],
        )
    elif task == "PourCup":
        generator.add_param(
            key="observation.modalities.obs.low_dim",
            name="task", 
            group=0, 
            values=[[
                "handPose",
                "handDofPos",
                "fullCupPose"
            ]],
            value_names=["PourCup"],
        )
    else:  # wrong task
        raise ValueError
    # number epochs
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )
    # data
    generator.add_param(
        key="train.data",
        name="", 
        group=0,
        values=[dataset],
    )
    # seed
    generator.add_param(
        key="train.seed",
        name="",
        group=0,
        values=[seed],
    )
    # variants
    generator.add_param(
        key="algo.critic.rnn.enabled",
        name="variant",
        group=1,
        values=[False, True, False],
        value_names=["vanilla", "rnn", "ext"],
    )
    generator.add_param(
        key="algo.ext.enabled",
        name="",
        group=1,
        values=[False, False, True],
    )
    # scanned hyperparameters
    if fixed:
        generator.add_param(
            key="algo.target_tau",
            name="",
            group=0,
            values=[0.005]
        )
        generator.add_param(
            key="algo.optim_params.critic.learning_rate.decay_factor",
            name="",
            group=0,
            values=[0.1]
        )
        generator.add_param(
            key="algo.optim_params.critic.learning_rate.epoch_schedule",
            name="",
            group=0,
            values=[[200, 400, 600, 800]]
        )
        generator.add_param(
            key="algo.optim_params.actor.learning_rate.decay_factor",
            name="",
            group=0,
            values=[0.1]
        )
        generator.add_param(
            key="algo.optim_params.actor.learning_rate.epoch_schedule",
            name="",
            group=0,
            values=[[200, 400, 600, 800]]
        )
        generator.add_param(
            key="algo.optim_params.critic.regularization.L2",
            name="",
            group=0,
            values=[0.0]
        )
        generator.add_param(
            key="algo.optim_params.actor.regularization.L2",
            name="",
            group=0,
            values=[0.0]
        )
        generator.add_param(
            key="algo.actor.max_gradient_norm",
            name="",
            group=0,
            values=[None]
        )
        generator.add_param(
            key="algo.critic.max_gradient_norm",
            name="",
            group=0,
            values=[None]
        )
        generator.add_param(
            key="algo.critic.use_huber",
            name="",
            group=0,
            values=[False]
        )
        generator.add_param(
            key="algo.critic.num_action_samples",
            name="",
            group=0,
            values=[1]
        )
    if task == "OpenDrawer":
        generator.add_param(
            key="train.batch_size",
            name="",
            group=0,
            values=[512]
        )
    else:
        generator.add_param(
            key="train.batch_size",
            name="",
            group=0,
            values=[1024]
        )
    return generator

def main(args):
    task = os.path.basename(args.dataset).split(".")[0]
    if args.fixed:
        # set seeds
        seed = 2055
    else:
        # choose seed
        seed = random.randint(0, 10000)
    # make config generator
    generator = make_generator(config_file=args.config, script_file=args.script, dataset=args.dataset, seed=seed, task=task, fixed=args.fixed)
    generator.generate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # Script name to generate
    parser.add_argument(
        "--script",
        type=str,
        help="path to output script that contains commands to run the generated training runs",
    )

    # path to datasets
    parser.add_argument(
        "--dataset",
        type=str,
        help="path to dataset to train on",
    )

    parser.add_argument(
        "--fixed",
        action="store_true",
        help="use values fixed in code",
    )

    args = parser.parse_args()
    main(args)
