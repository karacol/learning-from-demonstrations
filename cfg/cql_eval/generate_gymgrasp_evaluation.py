import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import os
import random
import glob

task_obs = {
    "OpenDrawer": {
        "default": ["drawerOpening", "handDofPos", "handPose"],
        "contacts": ["fingertipContactForces"],
        "fingertip": ["fingertipPos"],
        "velocities": ["jointVel"],
        "previous_acts": ["previousAction"],
        "misc": ["handlePos", "jointPos"]
    },
    "OpenDoor": {
        "default": ["handlePose", "handDofPos", "handPose"],
        "contacts": ["fingertipContactForces"],
        "fingertip": ["fingertipPos"],
        "velocities": ["jointVel"],
        "previous_acts": ["previousAction"],
        "misc": ["handlePos", "jointPos"]
    },
    "PourCup": {
        "default": ["fullCupPose", "handDofPos", "handPose"],
        "contacts": ["fingertipContactForces"],
        "fingertip": ["fingertipPos"],
        "velocities": ["jointVel"],
        "previous_acts": ["previousAction"],
        "misc": ["emptyCupPose", "jointPos"]
    },
    "LiftObject": {
        "default": ["objectPos", "objectRot", "handDofPos", "handPose"],
        "contacts": ["fingertipContactForces"],
        "fingertip": ["fingertipPos"],
        "velocities": ["jointVel", "objectLinVel", "objectAngVel"],
        "previous_acts": ["previousAction"],
        "misc": ["objectBboxBounds", "objectBboxCorners", "objectBboxPose", "objectSurfaceSamples", "jointPos"]
    }
}

batch_size = {
    "OpenDrawer": {
        "human": 512,
        "mg": 125,
    },
    "OpenDoor": {
        "human": 1024,
        "mg": 1024,
    },
    "PourCup": {
        "human": 1024,
        "mg": 256,
    },
    "LiftObject": {
        "human": 1024,
        "mg": 256,
    }
}

def add_generator_basis(generator, param_dict):
    for k, v in param_dict.items():
        generator.add_param(
            key=k,
            name="",
            group=0,
            values=[v],
        )
    return generator

def obs_study(config_file, script_file, data, seeds):
    """Generate config files for observation study. 1 experiment per task and seed, i.e. 12 in total."""
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # parameters
    basis = {
        "train.output_dir": "../thesis_obs_study_trained",
        "experiment.render": False,
        "experiment.render_video": False,
        "experiment.gymgrasp_recording": False,
        "experiment.rollout.enabled": True,
        "experiment.rollout.n": 20,
        "experiment.rollout.horizon": 300,
        "train.dataset_keys": ["actions", "rewards", "dones"],
        "train.num_epochs": 2000,
        "algo.critic.target_q_gap": 5,
        "algo.actor.layer_dims": [256, 512, 256],
        "algo.critic.layer_dims": [256, 512, 256],
        "algo.optim_params.actor.learning_rate.decay_factor": 0.1,
        "algo.optim_params.actor.learning_rate.epoch_schedule": [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800],
        "algo.optim_params.actor.learning_rate.initial": 0.0001,
        "algo.optim_params.critic.learning_rate.decay_factor": 0,
        "algo.optim_params.critic.learning_rate.initial": 3e-5,
        "algo.target_tau": 0.0005,
        "algo.optim_params.actor.regularization.L2": 0,
        "algo.optim_params.critic.regularization.L2": 0,
        "algo.critic.num_action_samples": 1,
        "algo.actor.max_gradient_norm": None,
        "algo.critic.max_gradient_norm": None,
        "train.seq_length": 1,
        "train.hdf5_normalize_obs": True,
        "experiment.validate": False,
    }

    generator = add_generator_basis(generator, basis)

    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )

    data_names = []
    data_paths = []
    obs_names = []
    obs_vals = []
    batch_sizes = []
    for t in data.keys():
        for os in [["default", "velocities"]]:
            data_names.append(t)
            data_paths.append(data[t])
            obs_names.append("".join([s[0] for s in os]))
            obs_val = []
            for o in os:
                obs_val += task_obs[t][o]
            obs_vals.append(obs_val)
            batch_sizes.append(batch_size[t]["human"])
    generator.add_param(
        key="train.data",
        name="task", 
        group=2,
        values=data_paths,
        value_names=data_names,
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="obs",
        group=2,
        values=obs_vals,
        value_names=obs_names
    )
    generator.add_param(
        key="train.batch_size",
        name="",
        group=2,
        values=batch_sizes,
    )

    generator.generate()

def extended_obs_study(config_file, script_file, data, seeds):
    """Generate config files for extended observation study. 4 experiments per task and seed, i.e. 48 in total."""
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # parameters
    basis = {
        "train.output_dir": "../thesis_obs_study_trained",
        "experiment.render": False,
        "experiment.render_video": False,
        "experiment.gymgrasp_recording": False,
        "experiment.rollout.enabled": True,
        "experiment.rollout.n": 20,
        "experiment.rollout.horizon": 300,
        "train.dataset_keys": ["actions", "rewards", "dones"],
        "train.num_epochs": 2000,
        "algo.critic.target_q_gap": 5,
        "algo.actor.layer_dims": [256, 512, 256],
        "algo.critic.layer_dims": [256, 512, 256],
        "algo.optim_params.actor.learning_rate.decay_factor": 0.1,
        "algo.optim_params.actor.learning_rate.epoch_schedule": [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800],
        "algo.optim_params.actor.learning_rate.initial": 0.0001,
        "algo.optim_params.critic.learning_rate.decay_factor": 0,
        "algo.optim_params.critic.learning_rate.initial": 3e-5,
        "algo.target_tau": 0.0005,
        "algo.optim_params.actor.regularization.L2": 0,
        "algo.optim_params.critic.regularization.L2": 0,
        "algo.critic.num_action_samples": 1,
        "algo.actor.max_gradient_norm": None,
        "algo.critic.max_gradient_norm": None,
        "train.seq_length": 1,
        "train.hdf5_normalize_obs": True,
        "experiment.validate": False,
    }

    generator = add_generator_basis(generator, basis)

    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )

    data_names = []
    data_paths = []
    obs_names = []
    obs_vals = []
    batch_sizes = []
    for t in data.keys():
        for os in [["default", "fingertip"], ["default", "contacts"], ["default", "previous_acts"], ["default", "velocities", "previous_acts"]]:
            data_names.append(t)
            data_paths.append(data[t])
            obs_names.append("".join([s[0] for s in os]))
            obs_val = []
            for o in os:
                obs_val += task_obs[t][o]
            obs_vals.append(obs_val)
            batch_sizes.append(batch_size[t]["human"])
    generator.add_param(
        key="train.data",
        name="task", 
        group=2,
        values=data_paths,
        value_names=data_names,
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="obs",
        group=2,
        values=obs_vals,
        value_names=obs_names
    )
    generator.add_param(
        key="train.batch_size",
        name="",
        group=2,
        values=batch_sizes,
    )

    generator.generate()

def variants_study(config_file, script_file, data, seeds):
    """Generate config files for CQL variants study. 3 experiments per task and seed, i.e. 36 in total."""
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # parameters
    basis = {
        "train.output_dir": "../thesis_variants_study_trained",
        "experiment.render": False,
        "experiment.render_video": False,
        "experiment.gymgrasp_recording": False,
        "experiment.rollout.enabled": True,
        "experiment.rollout.n": 20,
        "experiment.rollout.horizon": 300,
        "train.dataset_keys": ["actions", "rewards", "dones"],
        "train.num_epochs": 2000,
        "algo.critic.target_q_gap": 5,
        "algo.actor.layer_dims": [256, 512, 256],
        "algo.critic.layer_dims": [256, 512, 256],
        "algo.optim_params.actor.learning_rate.decay_factor": 0.1,
        "algo.optim_params.actor.learning_rate.epoch_schedule": [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800],
        "algo.optim_params.actor.learning_rate.initial": 0.0001,
        "algo.optim_params.critic.learning_rate.decay_factor": 0,
        "algo.optim_params.critic.learning_rate.initial": 3e-5,
        "algo.target_tau": 0.0005,
        "algo.optim_params.actor.regularization.L2": 0,
        "algo.optim_params.critic.regularization.L2": 0,
        "algo.critic.num_action_samples": 1,
        "algo.actor.max_gradient_norm": None,
        "algo.critic.max_gradient_norm": None,
        "train.hdf5_normalize_obs": True,
        "experiment.validate": False,
        "algo.ext.history_length": 2,
        "algo.actor.net.rnn.enabled": False,
        "algo.actor.net.rnn.hidden_dim": 256,
        "algo.actor.net.rnn.num_layers": 2,
        "algo.actor.net.rnn.horizon": 2,
        "algo.critic.rnn.hidden_dim": 256,
        "algo.critic.rnn.num_layers": 2,
        "algo.critic.rnn.horizon": 2,
        "algo.critic.rnn.shared": True,
    }

    generator = add_generator_basis(generator, basis)

    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )
    generator.add_param(
        key="train.data",
        name="task", 
        group=2,
        values=list(data.values()),
        value_names=[n+"-human" for n in data.keys()],
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="",
        group=2,
        values=[task_obs[t]["default"] for t in data.keys()],
    )
    generator.add_param(
        key="train.batch_size",
        name="",
        group=2,
        values=[batch_size[t]["human"] for t in data.keys()],
    )

    generator.add_param(
        key="train.seq_length",
        name="variant",
        group=3,
        values=[1, 2, 2],
        value_names=["vanilla", "ext", "rnn"],
    )
    # settings relevant for EXT variant
    generator.add_param(
        key="algo.ext.enabled",
        name="",
        group=3,
        values=[False, True, False],
    )
    # settings relevant for RNN variant
    generator.add_param(
        key="algo.critic.rnn.enabled",
        name="",
        group=3,
        values=[False, False, True],
    )

    generator.generate()

def mg_comparison(config_file, script_file, data, seeds):
    """Generate config files for CQL variants on mg data. 3 experiments per task and seed, i.e. 12 in total."""
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # parameters
    basis = {
        "train.output_dir": "../thesis_mg_study_trained",
        "experiment.render": False,
        "experiment.render_video": False,
        "experiment.gymgrasp_recording": False,
        "experiment.rollout.enabled": True,
        "experiment.rollout.n": 20,
        "experiment.rollout.horizon": 300,
        "train.dataset_keys": ["actions", "rewards", "dones"],
        "train.num_epochs": 2000,
        "algo.critic.target_q_gap": 5,
        "algo.target_tau": 0.0005,
        "algo.optim_params.actor.regularization.L2": 0,
        "algo.optim_params.critic.regularization.L2": 0,
        "algo.critic.num_action_samples": 1,
        "algo.actor.max_gradient_norm": None,
        "algo.critic.max_gradient_norm": None,
        "train.hdf5_normalize_obs": True,
        "experiment.validate": False,
        "algo.actor.layer_dims": [256, 256],
        "algo.critic.layer_dims": [256, 512, 256],
        "algo.optim_params.actor.learning_rate.decay_factor": 0.1,
        "algo.optim_params.actor.learning_rate.epoch_schedule": [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800],
        "algo.optim_params.actor.learning_rate.initial": 0.0001,
        "algo.optim_params.critic.learning_rate.decay_factor": 0,
        "algo.optim_params.critic.learning_rate.initial": 3e-5,
        "algo.ext.history_length": 2,
        "algo.actor.net.rnn.enabled": False,
        "algo.actor.net.rnn.hidden_dim": 256,
        "algo.actor.net.rnn.num_layers": 2,
        "algo.actor.net.rnn.horizon": 2,
        "algo.critic.rnn.hidden_dim": 256,
        "algo.critic.rnn.num_layers": 2,
        "algo.critic.rnn.horizon": 2,
        "algo.critic.rnn.shared": True,
    }

    generator = add_generator_basis(generator, basis)

    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )
    generator.add_param(
        key="train.data",
        name="task", 
        group=2,
        values=list(data.values()),
        value_names=[n+"-mg" for n in data.keys()],
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="",
        group=2,
        values=[task_obs[t]["default"] for t in data.keys()],
    )
    generator.add_param(
        key="train.batch_size",
        name="",
        group=2,
        values=[batch_size[t]["mg"] for t in data.keys()],
    )
    # variants
    generator.add_param(
        key="train.seq_length",
        name="variant",
        group=3,
        values=[1, 2, 2],
        value_names=["vanilla", "ext", "rnn"],
    )
    # settings relevant for EXT variant
    generator.add_param(
        key="algo.ext.enabled",
        name="",
        group=3,
        values=[False, True, False],
    )
    # settings relevant for RNN variant
    generator.add_param(
        key="algo.critic.rnn.enabled",
        name="",
        group=3,
        values=[False, False, True],
    )

    generator.generate()

def get_data(root, dir):
    dataset_pattern = os.path.join(root, dir, "**/*.hdf5")
    dataset_files = glob.glob(dataset_pattern, recursive=True)
    assert len(dataset_files) > 0
    datasets = {}
    for d in dataset_files:
        datasets[os.path.basename(d).split(".")[0]] = d
    return datasets

def main(args):
    human_data = get_data(args.data_root, "demo_datasets_fullinfo")
    mg_data = get_data(args.data_root, "demo_datasets_mg")
    # choose seed
    seeds = [random.randint(0, 10000) for _ in range(3)]
    obs_study(args.config, "obs_study.sh", human_data, seeds)
    extended_obs_study(args.config, "extended_obs_study.sh", human_data, seeds)
    variants_study(args.config, "human_variants_study.sh", human_data, seeds)
    mg_comparison(args.config, "mg_study.sh", mg_data, seeds)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # path to datasets
    parser.add_argument(
        "--data_root",
        type=str,
        help="root directory containing dataset directories",
    )

    args = parser.parse_args()
    main(args)
