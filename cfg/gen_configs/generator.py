import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse

class WrappedGenerator(object):
    """Wraps around HyperparamUtils.ConfigGenerator to help in incremental config generation.
    """
    def __init__(self, base_config_file, script_file) -> None:
        self.generator = HyperparamUtils.ConfigGenerator(
            base_config_file=base_config_file, script_file=script_file
        )
        self.no_groups = 0
    
    def add_param(self, key, name, group, values, value_names=None):
        self.generator.add_param(key, name, group, values, value_names)
        if group > self.no_groups:
            self.no_groups = group

    def generate(self):
        self.generator.generate()

def add_generator_bc_variants(generator):
    """Add parameter sweep for different BC variants (BC, BC-RNN-5, BC-RNN-10, BC-EXT-5, BC-EXT-10).

    Args:
        generator (WrappedGenerator): initialised config generator

    Returns:
        generator: modified WrappedGenerator object
    """
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["../bc_variants_trained"],
    )
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )
    variants_group = generator.no_groups + 1
    generator.add_param(
        key="train.seq_length", 
        name="", 
        group=variants_group, 
        values=[1, 2, 4, 6, 8], 
    )
    # MLP
    generator.add_param(
        key="algo.actor_layer_dims",
        name="mlp",
        group=variants_group,
        values=[(512, 512), (), (), (), ()],
        value_names=[512, 0, 0, 0, 0],
    )
    # RNN
    generator.add_param(
        key="algo.rnn.enabled",
        name="rnn", 
        group=variants_group, 
        values=[False, True, True, True, True],
    )
    generator.add_param(
        key="algo.rnn.horizon",
        name="rnnh", 
        group=variants_group, 
        values=[1, 2, 4, 6, 8], 
    )
    generator.add_param(
        key="algo.rnn.hidden_dim",
        name="", 
        group=0,
        values=[512], 
    )
    generator.add_param(
        key="algo.rnn.num_layers",
        name="", 
        group=0, 
        values=[2], 
    )    
    # EXT
    #generator.add_param(
    #    key="algo.ext.enabled",
    #    name="ext", 
    #    group=variants_group, 
    #    values=[False, False, False, True, True], 
    #)
    #generator.add_param(
    #    key="algo.ext.history_length",
    #    name="exth", 
    #    group=variants_group, 
    #    values=[1, 1, 1, 2, 4], 
    #)
    return generator

def add_generator_datasize(generator):
    """Add parameter sweep for different dataset sizes (20, 50, 100 percent).

    Args:
        generator (WrappedGenerator): initialised config generator

    Returns:
        generator: modified WrappedGenerator object
    """    
    datasize_group = generator.no_groups + 1
    # datasize
    generator.add_param(
        key="train.hdf5_filter_key",
        name="datasize",
        group=datasize_group,
        values=[20, 50, None],
        value_names=[20, 50, 100],
    )
    return generator    

def add_generator_bc(generator):
    """Add parameter sweep for different BC configurations (MLP size).

    Args:
        generator (WrappedGenerator): initialised config generator

    Returns:
        generator: modified WrappedGenerator object
    """ 
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["../bc_sweep_trained"],
    )
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )
    generator.add_param(
        key="algo.actor_layer_dims",
        name="mlp",
        group=1,
        values=[(1024, 1024), (256, 256)],
        value_names=[1024, 256],
    ) 
    return generator

def add_generator_bc_rnn(generator):
    """Add parameter sweep for different BC-RNN configurations (MLP size, horizon).

    Args:
        generator (WrappedGenerator): initialised config generator

    Returns:
        generator: modified WrappedGenerator object
    """
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["../bcrnn_sweep_trained"],
    )
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )
    generator.add_param(
        key="algo.rnn.enabled",
        name="",
        group=0, 
        values=[True],
    )    
    generator.add_param(
        key="train.seq_length", 
        name="", 
        group=1, 
        values=[2, 4, 6, 8],
    )    
    generator.add_param(
        key="algo.rnn.horizon",
        name="rnnh", 
        group=1, 
        values=[2, 4, 6, 8],
    )
    generator.add_param(
        key="algo.rnn.hidden_dim",
        name="", 
        group=0, 
        values=[512], 
    )
    #generator.add_param(
    #    key="algo.rnn.rnn_type",
    #    name="rnnt", 
    #    group=3, 
    #    values=["LSTM", "GRU"], 
    #)
    generator.add_param(
        key="algo.rnn.num_layers",
        name="", 
        group=0, 
        values=[2], 
    )
    generator.add_param(
        key="algo.actor_layer_dims",
        name="",
        group=0,
        values=[()],
        #value_names=[512, 256, 0],
    )
    return generator

def add_generator_bc_ext(generator):
    """Add parameter sweep for different BC-EXT configurations (MLP size, history).

    Args:
        generator (WrappedGenerator): initialised config generator

    Returns:
        generator: modified WrappedGenerator object
    """
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["../bcext_sweep_trained"],
    )    
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )    
    generator.add_param(
        key="algo.ext.enabled",
        name="", 
        group=0, 
        values=[True], 
    )    
    generator.add_param(
        key="train.seq_length", 
        name="", 
        group=1, 
        values=[2, 4, 6, 8, 10], 
    )
    generator.add_param(
        key="algo.ext.history_length",
        name="exth", 
        group=1, 
        values=[2, 4, 6, 8, 10], 
    )
    generator.add_param(
        key="algo.actor_layer_dims",
        name="mlp",
        group=2,
        values=[(1024, 1024), (256, 256)],
        value_names=[1024, 256],
    ) 
    return generator    

def make_generator(id, config_file, script_file, dataset, sweep_datasize):
    generator = WrappedGenerator(
        base_config_file=config_file, script_file=script_file
    )
    generator.add_param(
        key="train.data",
        name="", 
        group=0, 
        values=[dataset],
    )
    generator.add_param(
        key="experiment.render",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.render_video",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.rollout.enabled",
        name="", 
        group=0, 
        values=[True],
    )
    generator.add_param(
        key="experiment.rollout.n",
        name="", 
        group=0, 
        values=[20],
    )
    generator.add_param(
        key="experiment.rollout.horizon",
        name="", 
        group=0, 
        values=[300],
    )
    generator.add_param(
        key="train.dataset_keys",
        name="", 
        group=0, 
        values=[[
            "actions",
            "rewards",
            "dones"
        ]],
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="", 
        group=0, 
        values=[[
            "obs"
        ]],
    )

    if id == "bc-variants":
        generator = add_generator_bc_variants(generator)
    elif id == "bc-rnn":
        generator = add_generator_bc_rnn(generator)
    elif id == "bc-ext":
        generator = add_generator_bc_ext(generator)
    elif id == "bc":
        generator = add_generator_bc(generator)
    else:
        raise NotImplementedError

    if sweep_datasize:
        generator = add_generator_datasize(generator)
    
    return generator

def main(args):
    # make config generator
    generator = make_generator(
        id=args.id, config_file=args.config, script_file=args.script, dataset=args.dataset,
        sweep_datasize=args.datasize
    )
    # generate jsons and script
    generator.generate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config - will override any defaults.
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # Script name to generate - will override any defaults
    parser.add_argument(
        "--script",
        type=str,
        help="path to output script that contains commands to run the generated training runs",
    )

    # path to dataset - will override any defaults
    parser.add_argument(
        "--dataset",
        type=str,
        help="path to dataset to train on",
    )    

    parser.add_argument(
        "--id",
        type=str,
        default="bc-variants",
        help="which configs to generate, can be bc-variants (default), bc-rnn, bc-ext, bc"
    )

    parser.add_argument(
        "--datasize",
        type=bool,
        default=False,
        help="whether to sweep different dataset sizes (default False)"
    )

    args = parser.parse_args()
    main(args)
