import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import glob
import os
import random

def make_generator(config_file, script_file, seeds):
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # iterate through seeds
    generator.add_param(
        key="train.seed",
        name="seed", 
        group=1,
        values=seeds,
    )    
    
    return generator

def main(args):
    if args.fixed:
        # set seeds
        seeds = [3933, 5998, 7017]
    else:
        # choose 3 different seeds
        seeds = [random.randint(0, 10000) for _ in range(3)]
    # make config generator
    generator = make_generator(config_file=args.config, script_file=args.script, seeds=seeds)
    # generate jsons and script
    generator.generate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # Script name to generate
    parser.add_argument(
        "--script",
        type=str,
        help="path to output script that contains commands to run the generated training runs",
    )

    parser.add_argument(
        "--fixed",
        action="store_true",
        help="use values fixed in code",
    )

    args = parser.parse_args()
    main(args)
