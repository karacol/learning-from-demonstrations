import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import glob
import os
import random

def make_generator(config_file, script_file, datasets, seeds):
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    generator.add_param(
        key="train.output_dir",
        name="",
        group=0,
        values=["../bc_humanoids_trained"],
    )
    generator.add_param(
        key="experiment.render",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.render_video",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.gymgrasp_recording",
        name="", 
        group=0, 
        values=[False],
    )
    generator.add_param(
        key="experiment.rollout.enabled",
        name="", 
        group=0, 
        values=[True],
    )
    generator.add_param(
        key="experiment.rollout.n",
        name="", 
        group=0, 
        values=[20],
    )
    generator.add_param(
        key="experiment.rollout.horizon",
        name="", 
        group=0, 
        values=[300],
    )
    generator.add_param(
        key="train.dataset_keys",
        name="", 
        group=0, 
        values=[[
            "actions",
            "rewards",
            "dones"
        ]],
    )
    generator.add_param(
        key="observation.modalities.obs.low_dim",
        name="", 
        group=0, 
        values=[[
            "obs"
        ]],
    )

    # number epochs
    generator.add_param(
        key="train.num_epochs",
        name="",
        group=0,
        values=[1000],
    )

    # MLP like in RL: hidden layer sizes 512, 256, 256
    generator.add_param(
        key="algo.actor_layer_dims",
        name="", 
        group=0, 
        values=[(512, 256, 256)],
    )

    # iterate through datasets
    generator.add_param(
        key="train.data",
        name="task", 
        group=1,
        values=list(datasets.values()),
        value_names=list(datasets.keys()),
    )

    # iterate through seeds
    generator.add_param(
        key="train.seed",
        name="seed", 
        group=2,
        values=seeds,
    )    
    
    return generator

def main(args):
    # get datasets
    dataset_pattern = os.path.join(args.datasets, "*.hdf5")
    dataset_files = glob.glob(dataset_pattern)
    assert len(dataset_files) > 0
    datasets = {}
    for d in dataset_files:
        datasets[os.path.basename(d).split(".")[0]] = d
    if args.fixed:
        datasets_fixed = {"LiftObject": datasets["LiftObject"]}
        datasets = datasets_fixed
        # set seeds
        seeds = [3933, 5998, 7017]
    else:
        # choose 3 different seeds
        seeds = [random.randint(0, 10000) for _ in range(3)]
    # make config generator
    generator = make_generator(config_file=args.config, script_file=args.script, datasets=datasets, seeds=seeds)
    # generate jsons and script
    generator.generate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to base json config
    parser.add_argument(
        "--config",
        type=str,
        help="path to base config json that will be modified to generate jsons. The jsons will\
            be generated in the same folder as this file.",
    )

    # Script name to generate
    parser.add_argument(
        "--script",
        type=str,
        help="path to output script that contains commands to run the generated training runs",
    )

    # path to datasets
    parser.add_argument(
        "--datasets",
        type=str,
        help="path to folder with datasets to train on",
    )

    parser.add_argument(
        "--fixed",
        action="store_true",
        help="use values fixed in code",
    )

    args = parser.parse_args()
    main(args)
