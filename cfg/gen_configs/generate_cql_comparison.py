import robomimic
import robomimic.utils.hyperparam_utils as HyperparamUtils
import argparse
import glob
import os
import random

def make_generator(config_file, script_file):
    generator = HyperparamUtils.ConfigGenerator(
        base_config_file=config_file, script_file=script_file
    )
    # CQL, CQL-RNN, CQL-EXT
    generator.add_param(
        key="train.seq_length",
        name="", 
        group=1,
        values=[1, 10, 10],
    )
    generator.add_param(
        key="algo.actor.net.rnn.enabled",
        name="rnn", 
        group=1,
        values=[False, True, False],
    )
    generator.add_param(
        key="algo.actor.net.rnn.horizon",
        name="", 
        group=1,
        values=[1, 10, 1],
    )
    generator.add_param(
        key="algo.actor.layer_dims",
        name="", 
        group=1,
        values=[(300, 400), (), (300, 400)],
    )
    generator.add_param(
        key="algo.critic.rnn.enabled",
        name="", 
        group=1,
        values=[False, True, False],
    )
    generator.add_param(
        key="algo.critic.rnn.shared",
        name="",
        group=1,
        values=[False, True, False],
    )
    generator.add_param(
        key="algo.critic.rnn.horizon",
        name="", 
        group=1,
        values=[1, 10, 1],
    )
    generator.add_param(
        key="algo.actor.layer_dims",
        name="", 
        group=1,
        values=[(300, 400), (), (300, 400)],
    )
    generator.add_param(
        key="algo.ext.enabled",
        name="ext",
        group=1,
        values=[False, False, True]
    )
    generator.add_param(
        key="algo.ext.history_length",
        name="",
        group=1,
        values=[1, 1, 10]
    )
    return generator


def main(args):
    dir = os.path.dirname(os.path.abspath(__file__))
    config = os.path.join(dir, 'cql_lift_ph.json')
    # make config generator
    generator = make_generator(config_file=config, script_file=args.script)
    # generate jsons and script
    generator.generate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Script name to generate
    parser.add_argument(
        "--script",
        type=str,
        help="path to output script that contains commands to run the generated training runs",
    )

    args = parser.parse_args()
    main(args)
