# Learning from demonstrations

Different approaches to learning from human demonstrations collected in gym-grasp environments.

## Requirements

- [`robomimic` fork](https://github.com/karacolada/robomimic)
- gym-grasp demonstrations
- IsaacGym

The following steps have worked well to prepare my workspace for training:

```bash
# make directories
mkdir git
mkdir tools

# install miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.12.0-Linux-x86_64.sh
chmod u+x Miniconda3-py38_4.12.0-Linux-x86_64.sh
./Miniconda3-py38_4.12.0-Linux-x86_64.sh
source .bashrc

# need IsaacGym already in tools (download from website, transfer via scp)
cd tools
tar -xf IsaacGym_Preview_4_Package.tar.gz

# clone git repos
cd git
git clone git@git.ais.uni-bonn.de:mosbach/gym-grasp.git
git clone git@gitlab.com:karacol/learning-from-demonstrations.git
git clone git@github.com:karacolada/robomimic.git

# create conda env
conda create -n ig-mimic python=3.8 pyyaml scipy tensorboard opencv pytorch==1.11.0 torchvision==0.12.0 torchaudio==0.11.0 cudatoolkit=11.3 -c pytorch -c conda-forge
conda activate ig-mimic
cd ~/tools/isaacgym/python
pip install -e .
cd ~/git/gym-grasp
pip install -e .
cd ~/git/robomimic
pip install -e .
conda update ffmpeg

# if running Ubuntu 20.04, add this (adjust it to match your miniconda installation)
echo "export LD_LIBRARY_PATH=/home/xyz/miniconda3/envs/ig-mimic/lib" >> ~/.bashrc
```

## Dataset format

The demonstrations are stored in HDF5 files with the following structure:

```
└── <task_name>.hdf5
     └── data (group)
         ├── env_args (attribute): JSON string (keys env_name, env_type, env_kwargs)
         ├── total (attribute): number of observation-action samples in the dataset
         ├── demo_0 (group): one trajectory
         │   ├── num_samples (attribute): length of trajectory N, number of observation-action samples     
         │   ├── seed (attribute): seed used for environment generation
         │   ├── actions (dataset): environment actions, ordered by time, shape (N, A)
         │   ├── dones (dataset): done signal, shape (N,)
         │   ├── obs (group): observation keys
         │   │   ├── obs (dataset): full array of low_dim observation, shape (N, O)
         │   │   ├── fingertipContactForces (dataset): low_dim observation, shape (N, 15)
         │   │   ├── fingertipPos (dataset): low_dim observation, shape (N, 15)
         │   │   ├── handDofPos (dataset): low_dim observation, shape (N, 5)
         │   │   ├── handPose (dataset): low_dim observation, shape (N, 7)
         │   │   ├── jointPos (dataset): low_dim observation, shape (N, 17)
         │   │   ├── jointVel (dataset): low_dim observation, shape (N, 17)
         │   │   ├── previousAction (dataset): low_dim observation, shape (N, 11)
         │   │   └── <task-specific obs> (dataset): may be multiple, low_dim observation, shape (N, O_Task)
         │   ├── next_obs (group): next observation keys
         │   │   ├── obs (dataset): full array of low_dim observation, shape (N, O)
         │   │   ├── fingertipContactForces (dataset): low_dim observation, shape (N, 15)
         │   │   ├── fingertipPos (dataset): low_dim observation, shape (N, 15)
         │   │   ├── handDofPos (dataset): low_dim observation, shape (N, 5)
         │   │   ├── handPose (dataset): low_dim observation, shape (N, 7)
         │   │   ├── jointPos (dataset): low_dim observation, shape (N, 17)
         │   │   ├── jointVel (dataset): low_dim observation, shape (N, 17)
         │   │   ├── previousAction (dataset): low_dim observation, shape (N, 11)
         │   │   └── <task-specific obs> (dataset): may be multiple, low_dim observation, shape (N, O_Task)     
         │   ├── sparse_rewards (dataset): sparse environment rewards, ordered by time, shape (N,)
         │   └── dense_rewards (dataset): dense environment rewards, ordered by time, shape (N,)
         └── demo_1 (group): same as above
```

## Training

Select which reward signal to use by running `python3 select_rewards.py --dataset <file> --sparse <true/false>`.

Generate different dataset sizes using `python3 split_dataset_sizes.py --dataset <file>`.

Add train/validate masks using

```bash
python3 <path_to_robomimic>/robomimic/scripts/split_train_val.py --dataset <file> --ratio 0.1
python3 <path_to_robomimic>/robomimic/scripts/split_train_val.py --dataset <file> --ratio 0.1 --filter_key 20_percent
python3 <path_to_robomimic>/robomimic/scripts/split_train_val.py --dataset <file> --ratio 0.1 --filter_key 50_percent
```

Generate configuration files using

`python3 cfg/gen_configs/generator.py --config cfg/gen_configs/bc_base.json --script <path_to_robomimic>/robomimic/scripts/out.sh --dataset <file>`

Train running `./out.sh` in `<path_to_robomimic>/robomimic/scripts`.