"""
Script for querying Q-values for trained agents.
Example usage:

python3 investigate_cql_q_values.py --task LiftObject --variant vanilla --seed 6002 --demo_idx 0 --device cpu \
    --demo /home/karacol/git/gym-grasp/gym_grasp/demo_datasets_fullinfo/LiftObject/014_lemon/LiftObject.hdf5 \
    --results_path /home/karacol/Schreibtisch/thesis_results/can_touch/thesis_variants_study_trained
"""


import isaacgym

import argparse
import json
import h5py
import numpy as np
from copy import deepcopy

import torch
import h5py

import robomimic
import robomimic.utils.file_utils as FileUtils
import robomimic.utils.torch_utils as TorchUtils
import robomimic.utils.tensor_utils as TensorUtils

import os
import glob
from tqdm import tqdm
import json

fixed_epochs = [50, 500, 1000, 2000]

def get_data(dir):
    dataset_pattern = os.path.join(dir, "**/*.hdf5")
    dataset_files = glob.glob(dataset_pattern, recursive=True)
    assert len(dataset_files) > 0
    datasets = {}
    for d in dataset_files:
        datasets[os.path.basename(d).split(".")[0]] = d
    return datasets

def rec_fix_dict(d, key=None):
    """Recursive, in-place conversion of numpy arrays to lists.

    Args:
        d (dict): dictionary
        key (string): dictionary key to test next, used for recursion
    """
    if key is None:
        for k in d.keys():
            rec_fix_dict(d, k)
    else:
        if type(d[key]) == dict:
            for k in d[key].keys():
                rec_fix_dict(d[key], k)
        elif type(d[key]) == list:
            if type(d[key][0]) in [np.ndarray, torch.Tensor]:
                for i, e in enumerate(d[key]):
                    d[key][i] = e.tolist()
        elif type(d[key]) == np.ndarray:
            d[key] = d[key].tolist()

def store_data(store_dict, path):
    rec_fix_dict(store_dict)
    f = open(path+".json", "w")
    json.dump(store_dict, f, indent=4)

def run_q_values(args):
    if args.agent is not None:
        # relative path to agent
        ckpt_paths = [args.agent]
    else:
        ckpt_paths = glob.glob(os.path.join(args.results_path, args.task, args.variant, f"*_seed_{args.seed}_*", "20*", "models", "*"))

    # device
    if args.device == "cpu":
        device = TorchUtils.get_torch_device(try_to_use_cuda=False)
        device_id = -1
    else:  # cuda:i
        device_id = args.device.split(":")[1]
        device = TorchUtils.get_torch_device(try_to_use_cuda=True, id=device_id)

    demo_file = h5py.File(args.demo, "r")
    data = demo_file["data"]

    plot_dir = os.path.join(os.path.dirname(os.path.dirname(ckpt_paths[0])), "value_plots")
    try:
        os.mkdir(plot_dir)
    except FileExistsError:
        pass

    ptol = ["#EE7733", "#0077BB", "#009988"]

    if not args.all:
        plot_data = {e: {"q": [], "a": []} for e in fixed_epochs}
    else:
        plot_data = {"q": [], "a": [], "path": []}
    for ckpt_path in tqdm(ckpt_paths, desc="Playing trained models..."):
        if not args.all:
            if "model_epoch_" in ckpt_path:
                path_end = ckpt_path.split("/")[-1].split("_")
                epoch = int(path_end[path_end.index("epoch")+1].split('.')[0])
                if epoch not in fixed_epochs:
                    continue
                print(f"Playing model epoch {epoch}:")
        q, a, path = run_for_one_agent(ckpt_path, device, data, plot_dir, args.variant, args.all, args.demo_idx)
        if not args.all:
            plot_data[epoch]["q"] = q
            plot_data[epoch]["a"] = a
        else:
            plot_data["q"].append(q)
            plot_data["a"].append(a)
            plot_data["path"].append(path)
    print("")

    demo_file.close()

    if not args.all:
        titles = [f"Epoch {e}" for e in fixed_epochs]
        store_dict = {"data": plot_data, "path": path, "colors": ptol, "titles": titles}
        store_data(store_dict, path)
        print("Done.")
    else:
        store_dict = {"data": plot_data, "colors": ptol}
        store_data(store_dict, plot_data["path"][0])
        print("Done.")

def run_for_one_agent(ckpt_path, device, data, plot_dir, variant, all=False, demo_idx=None):
    # restore policy
    policy, chkpt_dict = FileUtils.policy_from_checkpoint(ckpt_path=ckpt_path, device=device, verbose=False)

    obs_keys = chkpt_dict["shape_metadata"]["all_obs_keys"]
    
    plot_path = os.path.join(plot_dir, f"Q_{chkpt_dict['env_metadata']['env_name']}_{variant}")

    if all:
        if "model_epoch_" in ckpt_path:
            path_end = ckpt_path.split("/")[-1].split("_")
            epoch_slice = path_end[path_end.index("epoch")+1]
            plot_path += f"_epoch_{epoch_slice.split('.')[0]}"    

    if demo_idx is not None:
        plot_path += f"_demo_{demo_idx}"
        obs = {}
        for obs_k in obs_keys:
            obs[obs_k] = data[f"demo_{demo_idx}/obs/{obs_k}"][:]
        obs = TensorUtils.to_device(TensorUtils.to_tensor(obs), device)
        acts = torch.Tensor(data[f"demo_{demo_idx}/actions"][:]).to(device)
        res_q, res_a = run_on_demo(policy, obs, acts, device)
        return res_q, res_a, plot_path
    else:
        all_qs = {"demo": [], "policy": [], "rand": []}
        all_acts = {"demo": [], "policy": [], "rand": []}
        for demo_k in tqdm(data.keys(), desc="Playing demos..."):
            obs = {}
            for obs_k in obs_keys:
                obs[obs_k] = data[f"{demo_k}/obs/{obs_k}"][:]
            obs = TensorUtils.to_device(TensorUtils.to_tensor(obs), device)
            acts = torch.Tensor(data[f"{demo_k}/actions"][:]).to(device)
            qs, acts_dict = run_on_demo(policy, obs, acts, device)
            all_qs["demo"].extend(qs["demo"])
            all_qs["policy"].extend(qs["policy"])
            all_qs["rand"].extend(qs["rand"])
            all_acts["demo"].extend(acts_dict["demo"])
            all_acts["policy"].extend(acts_dict["policy"])
            all_acts["rand"].extend(acts_dict["rand"])
        return all_qs, all_acts, plot_path 

def run_on_demo(policy, obs, acts, device):
    qs = {"demo": [], "policy": [], "rand": []}
    acts_dict = {"demo": [], "policy": [], "rand": []}
    for strategy in ["demo", "policy", "rand"]:
        policy.start_episode()
        for step in range(acts.shape[0]):
            ob = {}
            for obs_k in obs.keys():
                ob[obs_k] = obs[obs_k][step].unsqueeze(0)
            if strategy == "demo":
                act = acts[step]
            elif strategy == "policy":
                act = torch.Tensor(policy(deepcopy(ob))).squeeze().to(device)
            elif strategy == "rand":
                act = torch.FloatTensor(acts.shape[1]).uniform_(-1., 1.).to(device)
            q = policy.get_value(deepcopy(ob), deepcopy(act))
            qs[strategy].append(deepcopy(q.squeeze()))
            acts_dict[strategy].append(deepcopy(act))
    return (qs, acts_dict)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--results_path",
        type=str,
        required=True,
        help="path to results of training (output_dir)",
    )

    parser.add_argument(
        "--task",
        type=str,
        required=True,
        help="task to investigate",
    )

    parser.add_argument(
        "--variant",
        type=str,
        required=True,
        help="variant to investigate",
    )

    parser.add_argument(
        "--seed",
        type=int,
        required=True,
        help="seed to investigate",
    )

    parser.add_argument(
        "--demo",
        type=str,
        required=True,
        help="path to demo file",
    )

    # Path to trained model
    parser.add_argument(
        "--agent",
        type=str,
        help="(optional) path to specific saved checkpoint pth file",
    )

    parser.add_argument(
        "--demo_idx",
        type=int,
        help="(optional) index of demo to play through",
    )    

    parser.add_argument(
        "--device",
        type=str,
        default="cuda:0",
        help="(optional) torch device, default cuda:0",
    )

    parser.add_argument(
        "--all",
        action="store_true",
        help="(optional) run for all epochs instead of only 50, 500, 100, 2000",
    )

    args = parser.parse_args()
    run_q_values(args)
