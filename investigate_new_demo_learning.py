import isaacgym

import argparse
import json
import h5py
import numpy as np
from copy import deepcopy

import torch
import h5py

import robomimic
import robomimic.utils.file_utils as FileUtils
import robomimic.utils.torch_utils as TorchUtils
import robomimic.utils.tensor_utils as TensorUtils

from matplotlib import pyplot as plt

def run_policy_investigation(args):
    # relative path to agent
    ckpt_path = args.agent

    # device
    if args.device == "cpu":
        device = TorchUtils.get_torch_device(try_to_use_cuda=False)
        device_id = -1
    else:  # cuda:i
        device_id = args.device.split(":")[1]
        device = TorchUtils.get_torch_device(try_to_use_cuda=True, id=device_id)

    # restore policy
    policy, chkpt_dict = FileUtils.policy_from_checkpoint(ckpt_path=ckpt_path, device=device, verbose=True)

    policy.start_episode()

    obs_keys = chkpt_dict["shape_metadata"]["all_obs_keys"]
    
    plot_path = f"Policy_investigation_{chkpt_dict['env_metadata']['env_name']}"

    if "model_epoch_" in ckpt_path:
        path_end = ckpt_path.split("/")[-1].split("_")
        epoch_slice = path_end[path_end.index("epoch")+1]
        plot_path += f"_epoch_{epoch_slice.split('.')[0]}"    

    demo_file = h5py.File(args.demos, "r")
    data = demo_file["data"]
    if args.demo_idx is not None:
        plot_path += f"_demo_{args.demo_idx}"
        obs = {}
        for obs_k in obs_keys:
            obs[obs_k] = data[f"demo_{args.demo_idx}/obs/{obs_k}"][:]
        obs = TensorUtils.to_device(TensorUtils.to_tensor(obs), device)
        acts = torch.Tensor(data[f"demo_{args.demo_idx}/actions"][:]).to(device)
        obs_sum, res_a = run_on_demo(policy, obs, acts, device)
        plot(obs_sum, res_a, plot_path)
        plot_obs(obs, plot_path)
    else:
        all_obs_sum = []
        all_acts = {"demo": [], "policy": []}
        all_obs = []
        for demo_k in data.keys():
            obs = {}
            for obs_k in obs_keys:
                obs[obs_k] = data[f"{demo_k}/obs/{obs_k}"][:]
            obs = TensorUtils.to_device(TensorUtils.to_tensor(obs), device)
            acts = torch.Tensor(data[f"{demo_k}/actions"][:]).to(device)
            obs_sum, acts_dict = run_on_demo(policy, obs, acts, device)
            all_acts["demo"].extend(acts_dict["demo"])
            all_acts["policy"].extend(acts_dict["policy"])
            all_obs_sum.extend(obs_sum)
            all_obs.append(obs)
        plot(all_obs_sum, all_acts, plot_path)
        plot_obs(all_obs, plot_path)

def run_on_demo(policy, obs, acts, device):
    obs_sum = []
    acts_dict = {"demo": [], "policy": []}
    for step in range(acts.shape[0]):
        ob_sum = 0
        ob = {}
        for obs_k in obs.keys():
            ob[obs_k] = obs[obs_k][step].unsqueeze(0)
            ob_sum += ob[obs_k].sum()
        act_demo = acts[step]
        act_policy = torch.Tensor(policy(ob)).to(device)
        acts_dict["demo"].append(float(act_demo.sum()))
        acts_dict["policy"].append(float(act_policy.sum()))
        obs_sum.append(float(ob_sum))
    return (obs_sum, acts_dict)

def plot(obs_sum, acts, path):
    plt.figure(figsize=(8, 8))
    plt.grid()
    N = len(acts["demo"])
    area = (5 * np.ones(N))**2
    for k in acts.keys():
        plt.scatter(obs_sum, acts[k], s=area, label=k, alpha=0.25)
    plt.xlabel("observations: sum over all dimensions")
    plt.ylabel("actions: sum over all dimensions")
    plt.title("Trained policy")
    plt.legend()
    img_path = path + ".png"
    plt.savefig(img_path)

def plot_obs(obs, path):
    obs_cat = torch.cat([obs[obs_k] for obs_k in obs.keys()], dim=1)
    plt.figure(figsize=(8, 8))
    plt.grid()
    N = obs_cat.shape[0] * obs_cat.shape[1]
    area = (5 * np.ones(N))**2
    x_dims = np.concatenate([np.arange(obs_cat.shape[1]) for _ in range(obs_cat.shape[0])])
    plt.scatter(x_dims, obs_cat.flatten(), s=area, alpha=0.25)
    plt.xlabel("Dimension")
    plt.ylabel("Value")
    plt.title("Observations")
    img_path = path + "_obs.png"
    plt.savefig(img_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to trained model
    parser.add_argument(
        "--agent",
        type=str,
        required=True,
        help="path to saved checkpoint pth file",
    )

    parser.add_argument(
        "--demos",
        type=str,
        required=True,
        help="path to HDF5 file",
    )    

    parser.add_argument(
        "--demo_idx",
        type=int,
        help="(optional) index of demo to play through",
    )    

    parser.add_argument(
        "--device",
        type=str,
        default="cuda:0",
        help="(optional) torch device, default cuda:0",
    )

    args = parser.parse_args()
    run_policy_investigation(args)
