
variants study:
Maximum success rate: {"vanilla": {"OpenDrawer": [1.0, 1.0, 1.0], "OpenDoor": [1.0, 1.0, 0.85], "PourCup": [0.75, 0.8, 0.75], "LiftObject": [0.45, 0.55, 0.8]}, "ext": {"OpenDrawer": [1.0, 1.0, 1.0], "OpenDoor": [0.9, 0.9, 0.9], "PourCup": [0.55, 0.7, 0.55], "LiftObject": [0.6, 0.75, 0.65]}}
\begin{tabular} {@{}rccccc@{}}\toprule
    Method & \phantom{a} & OpenDrawer & OpenDoor & PourCup & LiftObject \\
    \midrule
    vanilla & & $\textbf{1.00} \pm \textbf{0.00}$ & $\textbf{0.95} \pm \textbf{0.07}$ & $\textbf{0.77} \pm \textbf{0.02}$ & $0.60 \pm 0.15 $ \\
    ext & & $\textbf{1.00} \pm \textbf{0.00}$ & $0.90 \pm 0.00 $ & $0.60 \pm 0.07 $ & $\textbf{0.67} \pm \textbf{0.06}$ \\
    \bottomrule
\end{tabular}

variants study:
Maximum success rate: {"rnn": {"OpenDrawer": [0.0, 0.0, 0.0], "OpenDoor": [0.0, 0.0, 0.0], "PourCup": [0.0, 0.0, 0.05], "LiftObject": [0.0, 0.0, 0.0]}}
\begin{tabular} {@{}rccccc@{}}\toprule
    Method & \phantom{a} & OpenDrawer & OpenDoor & PourCup & LiftObject \\
    \midrule
    rnn & & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.02} \pm \textbf{0.02}$ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    \bottomrule
\end{tabular}

mg study:
Maximum success rate: {"vanilla": {"OpenDrawer": [0.75, 0.8, 0.7], "OpenDoor": [0.65, 0.6, 0.45], "PourCup": [0.9, 0.9, 0.9], "LiftObject": [0.0, 0.0, 0.0]}, "ext": {"OpenDrawer": [0.3, 0.5, 0.55], "OpenDoor": [0.45, 0.5, 0.65], "PourCup": [0.65, 0.65, 0.55], "LiftObject": [0.0, 0.0, 0.0]}}
\begin{tabular} {@{}rccccc@{}}\toprule
    Method & \phantom{a} & OpenDrawer & OpenDoor & PourCup & LiftObject \\
    \midrule
    vanilla & & $\textbf{0.75} \pm \textbf{0.04}$ & $\textbf{0.57} \pm \textbf{0.08}$ & $\textbf{0.90} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    ext & & $0.45 \pm 0.11 $ & $0.53 \pm 0.08 $ & $0.62 \pm 0.05 $ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    \bottomrule
\end{tabular}

mg study:
Maximum success rate: {"rnn": {"OpenDrawer": [0.0, 0.1, 0.05], "OpenDoor": [0.0, 0.0, 0.0], "PourCup": [0.0, 0.0, 0.0], "LiftObject": [0.0, 0.0, 0.0]}}
\begin{tabular} {@{}rccccc@{}}\toprule
    Method & \phantom{a} & OpenDrawer & OpenDoor & PourCup & LiftObject \\
    \midrule
    rnn & & $\textbf{0.05} \pm \textbf{0.04}$ & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    \bottomrule
\end{tabular}

robomimic study:
Maximum success rate: {"rnn": {"can": [0.0, 0.0, 0.0], "lift": [0.15, 0.1, 0.2], "square": [0.0, 0.0, 0.0], "transport": [0.0, 0.0, 0.0], "tool_hang": [0.0, 0.0, 0.0]}, "ext": {"can": [0.35, 0.45, 0.35], "lift": [1.0, 1.0, 1.0], "square": [0.3, 0.25, 0.1], "transport": [0.0, 0.0, 0.0], "tool_hang": [0.0, 0.0, 0.0]}}
\begin{tabular} {@{}rcccccc@{}}\toprule
    Method & \phantom{a} & can & lift & square & transport & tool_hang \\
    \midrule
    rnn & & $0.00 \pm 0.00 $ & $0.15 \pm 0.04 $ & $0.00 \pm 0.00 $ & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    ext & & $\textbf{0.38} \pm \textbf{0.05}$ & $\textbf{1.00} \pm \textbf{0.00}$ & $\textbf{0.22} \pm \textbf{0.08}$ & $\textbf{0.00} \pm \textbf{0.00}$ & $\textbf{0.00} \pm \textbf{0.00}$ \\
    \bottomrule
\end{tabular}

obs study:
Maximum success rate: {"dc": {"OpenDrawer": [1.0, 1.0, 1.0], "OpenDoor": [0.9, 1.0, 0.8], "PourCup": [0.8, 0.6, 0.6], "LiftObject": [0.55, 0.4, 0.5]}, "dp": {"OpenDrawer": [0.75, 0.25, 0.3], "OpenDoor": [0.45, 0.5, 0.4], "PourCup": [0.1, 0.0, 0.0], "LiftObject": [0.2, 0.15, 0.15]}, "df": {"OpenDrawer": [1.0, 1.0, 1.0], "OpenDoor": [1.0, 1.0, 1.0], "PourCup": [0.9, 0.9, 1.0], "LiftObject": [0.75, 0.8, 0.65]}, "dv": {"OpenDrawer": [0.45, 0.75, 0.85], "OpenDoor": [0.35, 1.0, 0.75], "PourCup": [0.05, 0.0, 0.0], "LiftObject": [0.15, 0.1, 0.35]}, "dvp": {"OpenDrawer": [0.15, 0.25, 0.9], "OpenDoor": [0.5, 0.3, 0.65], "PourCup": [0.0, 0.0, 0.0], "LiftObject": [0.0, 0.1, 0.05]}}
\begin{tabular} {@{}rccccc@{}}\toprule
    Method & \phantom{a} & OpenDrawer & OpenDoor & PourCup & LiftObject \\
    \midrule
    dc & & $\textbf{1.00} \pm \textbf{0.00}$ & $0.90 \pm 0.08 $ & $0.67 \pm 0.09 $ & $0.48 \pm 0.06 $ \\
    dp & & $0.43 \pm 0.22 $ & $0.45 \pm 0.04 $ & $0.03 \pm 0.05 $ & $0.17 \pm 0.02 $ \\
    df & & $\textbf{1.00} \pm \textbf{0.00}$ & $\textbf{1.00} \pm \textbf{0.00}$ & $\textbf{0.93} \pm \textbf{0.05}$ & $\textbf{0.73} \pm \textbf{0.06}$ \\
    dv & & $0.68 \pm 0.17 $ & $0.70 \pm 0.27 $ & $0.02 \pm 0.02 $ & $0.20 \pm 0.11 $ \\
    dvp & & $0.43 \pm 0.33 $ & $0.48 \pm 0.14 $ & $0.00 \pm 0.00 $ & $0.05 \pm 0.04 $ \\
    \bottomrule
\end{tabular}
