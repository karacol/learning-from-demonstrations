import pandas as pd
from matplotlib import pyplot as plt
import json
import os
from matplotlib import rc
import matplotlib as mpl
import pyplot_themes as themes
import gc
import numpy as np

def smooth(scalars, weight=0.3):  # Weight between 0 and 1
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = list()
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value
    return smoothed

if __name__=="__main__":
    ptol_bright = ["#4477aa", "#66ccee", "#228833", "#ccbb44", "#ee6677", "#aa3377", "#bbbbbb"] #7
    ptol_vibrant = ["#0077bb", "#33bbee", "#009988", "#ee7733", "#cc3311", "#ee3377", "#bbbbbb"] #7
    ptol_vibrant_plus = ["#0077bb", "#33bbee", "#009988", "#ee7733", "#ddaa33", "#cc3311", "#ee3377", "#bbbbbb"]
    ptol_muted = ["#332288", "#88ccee", "#44aa99", "#117733", "#999933", "#ddcc77", "#cc6677", "#882255", "#aa4499", "#dddddd"] #10
    ptol_light = ["#77aadd", "#99ddff", "#44bb99", "#bbcc33", "#aaaa00", "#eedd88", "#ee8866", "#ffaabb", "#dddddd"] #9

    ptol = [ptol_vibrant[i] for i in [0, 3, 1, 4]]

    #rc('text.latex', preamble=r'\usepackage{cmbright}')
    plt.rcParams.update({
        "text.usetex": True,
        "mathtext.default": "regular",
        "font.family": "serif",
        #"font.sans-serif": ["Computer Modern Serif"],
        "font.size": 14
    })

    df = pd.read_csv("human-opendoor-seqlen.csv") 
    plt.figure(figsize=(6, 4))
    for i, seq_len in enumerate([2, 4, 10, 30]):
        plt.plot(df['logs/tb/global_step'], smooth(df[f'train.seq_length: {seq_len} - logs/tb/Rollout/Success_Rate/OpenDoor']), color=ptol[i], label=seq_len)
        plt.fill_between(df['logs/tb/global_step'], smooth(df[f'train.seq_length: {seq_len} - logs/tb/Rollout/Success_Rate/OpenDoor__MIN']), smooth(df[f'train.seq_length: {seq_len} - logs/tb/Rollout/Success_Rate/OpenDoor__MAX']), color=ptol[i], alpha=0.15)
    plt.xlim(0, 1000)
    plt.ylim(0, 1)
    plt.xlabel("Epochs")
    plt.ylabel("Success Rate")
    plt.legend(bbox_to_anchor=(0, 1.1, 1, 0), loc="center", ncol=4)
    plt.savefig("human-opendoor-ext-seqlen.pdf", format='pdf', bbox_inches='tight', transparent=True)

    df_runs = pd.read_csv("normal_all.csv")
    l = list(df_runs.columns)
    sweep_names = {"all": [], "true": [], "false": []}
    for i, e in enumerate(l):
        if e == "logs/tb/global_step":
            continue
        if e.endswith("logs/tb/Rollout/Success_Rate/OpenDoor"):
            sweep_name = e.split(" - ")[0]
            df_runs.rename(columns={e:sweep_name}, inplace=True)
            sweep_names["all"].append(sweep_name)
        else:
            df_runs.drop([e], axis=1, inplace=True)
    df_cat = pd.read_csv("normal_cat.csv")
    df_grouped = {"true": pd.DataFrame(df_runs["logs/tb/global_step"]), "false": pd.DataFrame(df_runs["logs/tb/global_step"])}
    for sweep_name in sweep_names["all"]:
        if df_cat.loc[(df_cat["Name"] == sweep_name)]["train.hdf5_normalize_obs"].values[0]:
            df_grouped["true"] = pd.concat([df_grouped["true"], df_runs[sweep_name]], axis=1)
            sweep_names["true"].append(sweep_name)
        else:
            df_grouped["false"] = pd.concat([df_grouped["false"], df_runs[sweep_name]], axis=1)
            sweep_names["false"].append(sweep_name)
    for k in df_grouped.keys():
        df_grouped[k]['mean'] = df_grouped[k][[sn for sn in sweep_names[k]]].mean(axis=1)
        df_grouped[k]['min'] = df_grouped[k][[sn for sn in sweep_names[k]]].min(axis=1)
        df_grouped[k]['max'] = df_grouped[k][[sn for sn in sweep_names[k]]].max(axis=1)

    labels={"true": "normalized", "false": "not normalized"}
    plt.figure(figsize=(6, 4))
    for i, k in enumerate(["true", "false"]):
        df = df_grouped[k]
        plt.plot(df['logs/tb/global_step'], smooth(df['mean']), color=ptol[i], label=labels[k])
        plt.fill_between(df['logs/tb/global_step'], smooth(df['min']), smooth(df['max']), color=ptol[i], alpha=0.15)
    plt.xlim(0, 1000)
    plt.ylim(0, 1)
    plt.xlabel("Epochs")
    plt.ylabel("Success Rate")
    plt.legend(bbox_to_anchor=(0, 1.1, 1, 0), loc="center", ncol=2)
    plt.savefig("human-opendoor-vanilla-normalise.pdf", format='pdf', bbox_inches='tight', transparent=True)


