import isaacgym

import argparse
import json
import h5py
import numpy as np
from copy import deepcopy

import torch
import h5py

import robomimic
import robomimic.utils.file_utils as FileUtils
import robomimic.utils.torch_utils as TorchUtils
import robomimic.utils.tensor_utils as TensorUtils

from matplotlib import pyplot as plt

def run_policy_investigation(args):
    # relative path to agent
    ckpt_path = args.agent

    # device
    if args.device == "cpu":
        device = TorchUtils.get_torch_device(try_to_use_cuda=False)
        device_id = -1
    else:  # cuda:i
        device_id = args.device.split(":")[1]
        device = TorchUtils.get_torch_device(try_to_use_cuda=True, id=device_id)

    # restore policy
    policy, chkpt_dict = FileUtils.policy_from_checkpoint(ckpt_path=ckpt_path, device=device, verbose=True)

    obs_keys = chkpt_dict["shape_metadata"]["all_obs_keys"]
    
    plot_path = "Obs_investigation"

    demo_file = h5py.File(args.demos, "r")
    data = demo_file["data"]
    for demo_k in data.keys():
        demo_plot_path = plot_path + f"_{demo_k}"
        obs = {}
        for obs_k in obs_keys:
            obs[obs_k] = data[f"{demo_k}/obs/{obs_k}"][:]
        obs = TensorUtils.to_device(TensorUtils.to_tensor(obs), device)
        full_obs = data[f"{demo_k}/obs/obs"][:]
        #plot_obs(obs, demo_plot_path)
        plot_full_obs(full_obs, demo_plot_path)

def plot_obs(obs, path):
    obs_cat = torch.cat([obs[obs_k] for obs_k in obs.keys()], dim=1)
    plt.figure(figsize=(8, 8))
    plt.grid()
    N = obs_cat.shape[0] * obs_cat.shape[1]
    area = (5 * np.ones(N))**2
    x_dims = np.concatenate([np.arange(obs_cat.shape[1]) for _ in range(obs_cat.shape[0])])
    plt.scatter(x_dims, obs_cat.flatten(), s=area, alpha=0.25)
    plt.xlabel("Dimension")
    plt.ylabel("Value")
    plt.title("Observations")
    img_path = path + ".png"
    plt.savefig(img_path)
    plt.close()

def plot_full_obs(full_obs, path):
    plt.figure(figsize=(8, 8))
    plt.grid()
    N = full_obs.shape[0] * full_obs.shape[1]
    area = (5 * np.ones(N))**2
    x_dims = np.concatenate([np.arange(full_obs.shape[1]) for _ in range(full_obs.shape[0])])
    plt.scatter(x_dims, full_obs.flatten(), s=area, alpha=0.25)
    plt.xlabel("Dimension")
    plt.ylabel("Value")
    plt.title("Observations")
    img_path = path + "_full.png"
    plt.savefig(img_path)
    plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Path to trained model
    parser.add_argument(
        "--agent",
        type=str,
        required=True,
        help="path to saved checkpoint pth file",
    )

    parser.add_argument(
        "--demos",
        type=str,
        required=True,
        help="path to HDF5 file",
    )

    parser.add_argument(
        "--device",
        type=str,
        default="cuda:0",
        help="(optional) torch device, default cuda:0",
    )

    args = parser.parse_args()
    run_policy_investigation(args)
