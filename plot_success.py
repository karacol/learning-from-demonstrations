import pandas as pd
from matplotlib import pyplot as plt
import json
import os
from matplotlib import rc
import matplotlib as mpl
import pyplot_themes as themes
import gc
import numpy as np

def smooth(scalars, weight=0.3):  # Weight between 0 and 1
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = list()
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value
    return smoothed

def get_df(root, study_folder, task, variant, tasks_dict):
    runs = os.listdir(os.path.join(root, study_folder, task, variant))
    df = None
    for i, run in enumerate(runs):
        with open(os.path.join(root, study_folder, task, variant, run, "scalars.json"), "r") as f:
            if df is None:
                df = pd.read_json(f, orient="records")
            else:
                tmp = pd.read_json(f, orient="records")
                df = df.merge(tmp[['step', f'Rollout/Success_Rate/{tasks_dict[task]}']])
        df.rename(columns={f'Rollout/Success_Rate/{tasks_dict[task]}':f'success_{i}'}, inplace=True)
    df['mean'] = df[[f'success_{i}' for i in range(len(runs))]].mean(axis=1)
    df['min'] = df[[f'success_{i}' for i in range(len(runs))]].min(axis=1)
    df['max'] = df[[f'success_{i}' for i in range(len(runs))]].max(axis=1)
    return df

def plot_suplots(dfs, colors, labels, path, nrows, ncols, legendpad, hidden_labels=[], baseline=None):
    fig, axes = plt.subplots(nrows, ncols, sharey=True, sharex=True, figsize=(2*ncols, 2*nrows))
    ls = []
    if baseline is not None:
        baseline_color = colors[0]
        colors = colors[1:]
    for j, task in enumerate(dfs.keys()):
        if nrows == 1:
            ax = axes[j]
        else:
            ax = axes[int((j-(j%ncols))/ncols)][j%ncols]
        ax.grid(True)
        ax.set_title(task, fontsize=10)
        ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True)
        if baseline is not None:
            baseline_color
            l = ax.plot(dfs[task][0]['step'], np.full(dfs[task][0]['step'].shape, baseline[task]), color=baseline_color)
            if j == 0:
                ls.append(l[0])
        for i, df in enumerate(dfs[task]):
            if labels[i] in hidden_labels:
                l = ax.plot(df['step'], smooth(df['mean']), color=colors[i], linestyle='None')
            else:
                l = ax.plot(df['step'], smooth(df['mean']), color=colors[i])
                ax.fill_between(df['step'], smooth(df['min']), smooth(df['max']), color=colors[i], alpha=0.15)
            if j == 0:
                ls.append(l[0])
    if j < (nrows*ncols-1):
        for j in range(j+1, nrows*ncols):
            if nrows == 1:
                ax = axes[j]
            else:
                ax = axes[int((j-(j%ncols))/ncols)][j%ncols]
            ax.set_frame_on(False)
            ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    if nrows > 1:
        plt.subplots_adjust(hspace=0.4)
    plt.ylim(-0.01, 1.01)
    plt.xlim(0, 2000)
    plt.xticks([0, 500, 1000, 1500, 2000])
    # add a big axes, hide frame
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.grid(False)
    plt.xlabel("Epoch")
    plt.ylabel("Success Rate", labelpad=10)    
    if baseline is not None:
        labels = ["CQL (Max)"] + labels
    plt.legend(ls, labels, bbox_to_anchor=(0, 1+legendpad, 1, 0), loc="center", ncol=4)
    plt.savefig(path, format='pdf', bbox_inches='tight', transparent=True)


def plot_dfs(dfs, colors, labels, path, legendpad):
    plt.figure(figsize=(4, 4))
    plt.grid()
    for i, df in enumerate(dfs):
        plt.plot(df['step'], smooth(df['mean']), color=colors[i], label=labels[i])
        plt.fill_between(df['step'], smooth(df['min']), smooth(df['max']), color=colors[i], alpha=0.15)
    plt.xlabel("Epoch")
    plt.ylabel("Success Rate")
    plt.ylim(-0.01, 1.01)
    plt.xlim(0, 2000)
    plt.xticks([0, 500, 1000, 1500, 2000])
    plt.legend(bbox_to_anchor=(0, 1+legendpad, 1, 0), loc="center", ncol=2)
    plt.savefig(path, format='pdf', bbox_inches='tight', transparent=True)

if __name__ == "__main__":
    root = "/home/karacol/Schreibtisch/thesis_results"
    
    ptol_bright = ["#4477aa", "#66ccee", "#228833", "#ccbb44", "#ee6677", "#aa3377", "#bbbbbb"] #7
    ptol_vibrant = ["#0077bb", "#33bbee", "#009988", "#ee7733", "#cc3311", "#ee3377", "#bbbbbb"] #7
    ptol_vibrant_plus = ["#0077bb", "#33bbee", "#009988", "#ee7733", "#ddaa33", "#cc3311", "#ee3377", "#bbbbbb"]
    ptol_muted = ["#332288", "#88ccee", "#44aa99", "#117733", "#999933", "#ddcc77", "#cc6677", "#882255", "#aa4499", "#dddddd"] #10
    ptol_light = ["#77aadd", "#99ddff", "#44bb99", "#bbcc33", "#aaaa00", "#eedd88", "#ee8866", "#ffaabb", "#dddddd"] #9

    ptol = ptol_vibrant_plus

    #rc('text.latex', preamble=r'\usepackage{cmbright}')
    plt.rcParams.update({
        "text.usetex": True,
        "mathtext.default": "regular",
        "font.family": "serif",
        #"font.sans-serif": ["Computer Modern Serif"],
        "font.size": 9
    })

    # gym-grasp tasks
    #tasks_dict = {"OpenDrawer": "OpenDrawer", "OpenDoor": "OpenDoor", "PourCup": "PourCup", "LiftObject": "LiftObject"}
    #tasks_dfs = {}
    ## human tasks
    #for task in tasks_dict.keys():
    #    study_folder = "thesis_variants_study_trained"
    #    dfs = []
    #    for variant in ["vanilla", "ext", "rnn"]:
    #        dfs.append(get_df(root, study_folder, task, variant, tasks_dict))
    #    study_folder = "thesis_obs_study_trained"
    #    for variant in ["dv", "dp", "dvp", "dc", "df"]:
    #        dfs.append(get_df(root, study_folder, task, variant, tasks_dict))
    #    labels = ["CQL", "CQL-EXT", "CQL-RNN", "velocities", "previous actions", "velocities, previous actions", "finger contacts", "fingertip positions"]
    #    plot_dfs(dfs, ptol, labels, os.path.join(root, f"{task}_human_success.pdf"), 0.2)
    #    tasks_dfs[task] = dfs
    #plot_suplots(tasks_dfs, ptol, labels, os.path.join(root, f"human_success.pdf"), 1, 4, 0.3)
    
    # mg tasks
    #tasks_dfs = {}
    #for task in tasks_dict.keys():
    #    study_folder = "thesis_mg_study_trained"
    #    dfs = []
    #    for variant in ["vanilla", "ext", "rnn"]:
    #        dfs.append(get_df(root, study_folder, task, variant, tasks_dict))
    #    labels = ["CQL", "CQL-EXT", "CQL-RNN"]
    #    plot_dfs(dfs, ptol, labels, os.path.join(root, f"{task}_mg_success.pdf"), 0.1)
    #    tasks_dfs[task] = dfs
    #plot_suplots(tasks_dfs, ptol, labels, os.path.join(root, f"mg_success.pdf"), 1, 4, 0.25)

    # robomimic tasks
    tasks_dfs = {}
    tasks_dict = {"can": "PickPlaceCan", "lift": "Lift", "square": "NutAssemblySquare", "transport": "TwoArmTransport", "tool_hang": "ToolHang"}
    for task in tasks_dict.keys():
        study_folder = "thesis_robomimic_study_trained"
        dfs = []
        for variant in ["ext", "rnn"]:
            dfs.append(get_df(root, study_folder, task, variant, tasks_dict))
        labels = ["CQL-EXT", "CQL-RNN"]
        #plot_dfs(dfs, ptol, labels, os.path.join(root, f"{task}_robomimic_success.pdf"), 0.1)
        tasks_dfs[task] = dfs
    plot_suplots(tasks_dfs, ptol, labels, os.path.join(root, f"robomimic_success.pdf"), 2, 3, 0.14)