from tensorboard.backend.event_processing import event_accumulator
import pandas as pd
import os
import glob
import json
import numpy as np
import math

def parse_tensorboard(path, scalars):
    """returns a dictionary of pandas dataframes for each requested scalar"""
    ea = event_accumulator.EventAccumulator(
        path,
        size_guidance={event_accumulator.SCALARS: 0},
    )
    _absorb_print = ea.Reload()
    # make sure the scalars are in the event accumulator tags
    assert all(
        s in ea.Tags()["scalars"] for s in scalars
    ), "some scalars were not found in the event accumulator"
    df = pd.DataFrame(ea.Scalars(scalars[0]))
    df.rename(columns={'value':scalars[0]}, inplace=True)
    df.drop('wall_time', axis=1, inplace=True)
    if len(scalars) > 1:
        for k in scalars[1:]:
            tmp = pd.DataFrame(ea.Scalars(k))
            tmp.rename(columns={'value':k}, inplace=True)
            df = df.merge(tmp[['step', k]])
    return df

def latex_print(success, tasks, variants):
    averaged = {v: {t: {} for t in tasks} for v in variants}
    max_per_task = {t: 0.0 for t in tasks}
    print(f"\\begin{{tabular}} {{@{{}}rc{'c'*len(tasks)}@{{}}}}\\toprule")
    print(f"    Method & \\phantom{{a}} & {' & '.join(tasks)} \\\\")
    print(f"    \\midrule")
    for v in variants:
        for t in tasks:
            averaged[v][t]["mean"] = round(np.mean(success[v][t]), 2)
            averaged[v][t]["std"] = round(np.std(success[v][t]), 2)
            if averaged[v][t]["mean"] > max_per_task[t]:
                max_per_task[t] = averaged[v][t]["mean"]
    for v in variants:
        row_str = f"    {v} &"
        for t in tasks:
            if math.isclose(averaged[v][t]["mean"], max_per_task[t]):
                row_str += f" & $\\textbf{{{averaged[v][t]['mean']:.2f}}} \\pm \\textbf{{{averaged[v][t]['std']:.2f}}}$"
            else:
                row_str += f" & ${averaged[v][t]['mean']:.2f} \\pm {averaged[v][t]['std']:.2f} $"
        row_str += " \\\\"
        print(row_str)
    print("    \\bottomrule")
    print(f"\\end{{tabular}}")

def thesis_study(path):
    study_name = " ".join(os.path.basename(path).split("_")[1:-1])
    print(f"\n{study_name}:")
    if study_name == "robomimic study":
        tasks = {"can": "PickPlaceCan", "lift": "Lift", "square": "NutAssemblySquare", "transport": "TwoArmTransport", "tool_hang": "ToolHang"}
    else:
        tasks = {"OpenDrawer": "OpenDrawer", "OpenDoor": "OpenDoor", "PourCup": "PourCup", "LiftObject": "LiftObject"}
    task_list = list(tasks.keys())
    variants = os.listdir(os.path.join(path, task_list[0]))
    max_success_rate = {v: {t: [] for t in tasks} for v in variants}
    for task in task_list:
        for variant in variants:
            runs = os.listdir(os.path.join(path, task, variant))
            for run in runs:
                scalars = [f"Rollout/Success_Rate/{tasks[task]}", f"Rollout/Success_Rate/{tasks[task]}-max"]
                event_file = glob.glob(os.path.join(path, task, variant, run, "*/logs/tb/events.out.tfevents*"))[0]
                df = parse_tensorboard(event_file, scalars)
                df.to_json(os.path.join(path, task, variant, run, "scalars.json"), "records", indent=4)
                max_success_rate[variant][task].append(round(df[f"Rollout/Success_Rate/{tasks[task]}-max"].iloc[-1], 2))
    print("Maximum success rate:", json.dumps(max_success_rate))
    latex_print(max_success_rate, task_list, variants)

if __name__ == "__main__":
    path = "/home/karacol/Schreibtisch/thesis_results/new_data"
    thesis_study(os.path.join(path, "thesis_variants_study_trained"))
    #thesis_study(os.path.join(path, "thesis_mg_study_trained"))
    #thesis_study(os.path.join(path, "thesis_robomimic_study_trained"))
    #thesis_study(os.path.join(path, "thesis_obs_study_trained"))